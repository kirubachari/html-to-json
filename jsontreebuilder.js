/* $Id:$ */
var JsonTreeBuilder = ( function(){

	var _elemConfig,
		_stylesConfig,
		_infoToExclude;
	
	var _getNodeInfo = function( node ){
		
		var _cont = null, _style = {}, _type, 
			nodeName = node.nodeName.toLowerCase(), contentStyleJson = {};
		
		switch( nodeName ){
			case "span"		:	//NO I18N
								_cont = node.textContent;
								_type = NODE_TYPE.TEXT;
								break;
			
			case "a"		:	//NO I18N
								_type = "anchor"; //dummy//No I18N
								break;
				
			case "br"		:	//No I18N
								_cont = UNICODE.BR,
								_type = NODE_TYPE.BR;	
								break;
			
			case "table"	:	//No I18N
								_cont = UNICODE.TABLE,
								_type = NODE_TYPE.TABLE;
								break;
						
			case "th"		:	//No I18N
			case "td"		:	//No I18N
								_cont = UNICODE.CELL,
								_type = NODE_TYPE.CELL;
								break;
			
			case "tr"		:	//No I18N
								_cont = UNICODE.ROW,
								_type = NODE_TYPE.ROW;
								break;
						
			case "hr"		:	//No I18N
								_cont = UNICODE.HR,
								_type = NODE_TYPE.HR;	
								break;
						
			case "img"		:	//No I18N
								_cont = UNICODE.IMAGE,
								_type = NODE_TYPE.IMAGE;
								break;
						
			case "iframe"	:	//No I18N
								_cont = UNICODE.IMAGE,
								_type = NODE_TYPE.WIDGET;
								break;
								
			case "p"		:	//No I18N
								_cont = UNICODE.PARA;
								_type = NODE_TYPE.PARAGRAPH;	
								break;
								
			case "body"		:	//NO I18N
								_cont = null;
								_type = NODE_TYPE.BODY;
								break;
		}
		
		extend( _style, HTMLUtil.getDataSet( node ) );
		
		contentStyleJson = { 
				"cont"	:	_cont,//No I18N
				"style"	:	_style,//No I18N
				"type"	:	_type//No I18N
		};
		
		return contentStyleJson;
		
	},

	isCallbackHas = function( callback, handler ){
		return Boolean( callback && callback[ handler ] && 
						typeof( callback[ handler ] ) === "function" );
	},
    
    _getUnAllowedElems = function( config ){
    	
    	var elems = [];
    	
    	for( var key in config ){
    		var obj = config[ key ];
    		if( obj.hasOwnProperty( 'isAllowed' ) && obj.isAllowed == false ){
    			elems.push( key );
    		}
    	}
    	
    	return elems;
    },
    
    _getElementsConfig = function( config ){
    	
    	typeof PubSub != "undefined" &&  PubSub.publish("JSONTreeBuilder/ElementConfig",[ config, _elemConfig ]);//no i18n
    	
    },
    
    _getStylesConfig = function( config ){
    	
    	typeof PubSub != "undefined" &&  PubSub.publish("JSONTreeBuilder/StylesConfig",[ config, _stylesConfig, _infoToExclude ]);//no i18n
    	
    },
    
    _refineStyleObj = function( nodeInfo ){
    	
    	
    	var styleObj = nodeInfo.style,
    		keys = Object.keys( styleObj ),
	    	_keysToRemove = Utils.getCommonKeys( keys, _infoToExclude),
			_keysLength = _keysToRemove.length;
    	
    	for( var key in styleObj ){
    		
    		if( !_stylesConfig.hasOwnProperty( key )){
    			continue;	
    		}
    		
    		for( var _idx = 0,_len = _stylesConfig[ key ].length; _idx < _len; _idx++){
				delete styleObj[ key ][ _stylesConfig[ key ][ _idx ] ];
			}
    		
    		typeof PubSub != "undefined" &&  PubSub.publish("JSONTreeBuilder/styleObj",[ styleObj, _stylesConfig ]);//no i18n

    	}
    	
    	if( _keysLength ){
			
			for( var idx = 0, len = _keysLength; idx < len; idx++ ){
				delete styleObj[ _keysToRemove[ idx ] ];
			}
			
		}
    },

	getDOMTreeJSON = function( node, callback ){
		
		var nodeInfo = _getNodeInfo( node ),
		childNodes = node.children,
		len = childNodes.length;
		
		_refineStyleObj( nodeInfo );
		
		isCallbackHas( callback, 'startHandler' ) && callback.startHandler( nodeInfo ); //NO I18N
		if( len ){
			nodeInfo.children = [];
			for( var idx = 0; idx < len; idx++ ){
				var child = getDOMTreeJSON( childNodes[ idx ], callback );
				nodeInfo.children.push( child )
			}
		}
		isCallbackHas( callback, 'endHandler' ) && callback.endHandler( nodeInfo ); //NO I18N
		
		return nodeInfo;
	},

	parseHTML = function( arg, callback ){
		var HTMLNode = arg[0],
			config = arg[1] ? arg[1] : {}; 
		
		_elemConfig = {},
		_stylesConfig = {},
		_infoToExclude = [];
		
		_getElementsConfig( config );
		_getStylesConfig( config );
		
		processedEle = HTMLPreProcessor.getProcessedHTML( HTMLNode, _elemConfig );
		return getDOMTreeJSON( processedEle, callback );
	};

	return {
		parseHTML 	: 	parseHTML
	};

})();