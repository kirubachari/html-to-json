/* $Id$ */
Utils = ( function(){
	"use strict";//no i18n
	
	var chars = "abcdefghijklmnopqrstuvwxyz0123456789", //No I18N
	
	randomString = function(len) {
	    var string_length = len || 12;
	    var randomString = '';
	    for (var i = 0; i < string_length; i++) {
	        var randomNum = Math.floor(Math.random() * chars.length);
	        randomString += chars.substring(randomNum, randomNum + 1);
	    }
	    return randomString;
	},
	
	getRandomNumber = function(min, max) {
	    min = min || 10000000;
	    max = max || 99999999;
	    //8 digit number
	    return parseInt(Math.random() * (max - min) + min);
	},
	
	uniqueArray = function( arr ){
		var resultArr;
		
		if( !Array.isArray( arr )){
			return null;
		}
		
		arr.forEach(function( ele, idx ){
			!resultArr.contains( ele ) ? resultArr.push( ele ) : null;
		});
		
		return resultArr;
	},
	
	getCommonKeys = function( arr1, arr2 ){
		var res = [];
		for( var _idx = 0, len = arr1.length; _idx < len; _idx++ ){
			var val = arr1[ _idx ];
			arr2.indexOf( val ) != -1 && res.push( val );
		}
		return res;
	};

	return {
		randomString	:	randomString,
		getRandomNumber	:	getRandomNumber,
		uniqueArray		:	uniqueArray,
		getCommonKeys	:	getCommonKeys
	}
	
})();