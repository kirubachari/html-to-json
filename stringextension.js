/*$Id$*/

//String related self-executive block
(function() {
	
    //String.prototype extensions
    
    String.prototype.capitalize = function() {
        return this.replace(/(?:^|\s)\S/g, function(a) {
            return a.toUpperCase();
        });
    };
    
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
    
    String.prototype.splitWithIndex=function(delim){
    	var ret=[];
        var splits=this.split(delim);
        var index=0;
        for(var i=0;i<splits.length;i++){
        	ret.push([index,splits[i]]);
            index+=splits[i].length+delim.length;
    	}
        return ret;
    };
    
    var convertedValue = {}; 
    // 1in = 2.54cm = 25.4mm = 72pt = 864pc = ppi*1px
    String.prototype.convertor = function() {
        var value = parseFloat(this);
        if ( convertedValue.hasOwnProperty(this)) {
            var c = convertedValue[this];
            var units = {inches: c.inches, pixels: c.pixels, centimeters: c.centimeters, millimeters: c.millimeters, points: c.points, picas: c.picas};
            return units;
        } else {
            var units = {inches: value, pixels: value, centimeters: value, millimeters: value, points: value, picas: value};
        }
        if (!/^[-+0-9]/.test(this) || !/px|in|cm|mm|pt|pc$/i.test(this)) {
            return units;
        }

        var ppi = 96;
        if (/px$/i.test(this)) {
            // 1/ppi in = 2.54/ppi cm = 25.4/ppi mm = 72/ppi pt = 864/ppi pc = 1px
            units.inches *= 1 / ppi;
            units.centimeters *= 2.54 / ppi;
            units.millimeters *= 25.4 / ppi;
            units.points *= 72 / ppi;
            units.picas *= 864 / ppi;
        } else if (/in$/i.test(this)) {
            // 1 in = 2.54 cm = 25.4 mm = 72 pt = 864 pc = ppi px
            units.centimeters *= 2.54;
            units.millimeters *= 25.4;
            units.points *= 72;
            units.picas *= 864;
            units.pixels *= ppi;
        } else if (/cm$/i.test(this)) {
            // 1/2.54 in = 2.54/2.54 cm = 25.4/2.54 mm = 72/2.54 pt = 864/2.54 pc = ppi/2.54 px
            units.inches *= 1 / 2.54;
            units.millimeters *= 10;
            units.points *= 72 / 2.54;
            units.picas *= 864 / 2.54;
            units.pixels *= ppi / 2.54;
        } else if (/mm$/i.test(this)) {
            // 1/25.4 in = 2.54/25.4 cm = 25.4/25.4 mm = 72/25.4 pt = 864/25.4 pc = ppi/25.4 px
            units.inches *= 1 / 25.4;
            units.centimeters *= 0.1;
            units.points *= 72 / 25.4;
            units.picas *= 864 / 25.4;
            units.pixels *= ppi / 25.4;
        } else if (/pt$/i.test(this)) {
            // 1/72 in = 2.54/72 cm = 25.4/72 mm = 72/72 pt = 864/72 pc = ppi/72 px
            units.inches *= 1 / 72;
            units.centimeters *= 2.54 / 72;
            units.millimeters *= 25.4 / 72;
            units.picas *= 12;
            units.pixels *= ppi / 72;
        } else if (/pc$/i.test(this)) {
            // 1/864 in = 2.54/864 cm = 25.4/864 mm = 72/864 pt = 864/864 pc = ppi/864 px
            units.inches *= 1 / 864;
            units.centimeters *= 2.54 / 864;
            units.millimeters *= 25.4 / 864;
            units.points *= 1 / 12;
            units.pixels *= ppi / 864;
        }
        //units.pixels = Math.round(units.pixels);
        convertedValue[this] = ZWJSONUtils.clone( units );
        return units;
    };
    
    //TODO: To be discussed with @Sivaguru by @Gokul
    var UNIT_VALUE_PER_INCH = {
    		"px": 96,//no i18n
    		"in": 1,//no i18n
    		"cm": 2.54,//no i18n
    		"mm": 25.4,//no i18n
    		"pt": 72,//no i18n
    		"pc": 862//no i18n
    	};
    
    String.prototype.convertUnitTo = function(newUnit) {
    	    var value = parseFloat(this);
    	    var oldUnitIndex = this.search(/(px|in|cm|mm|pt|pc)$/i);
    	    if (value == NaN || oldUnitIndex == -1){
    	        return this;
    	    }
    	    var oldUnit = this.substring(oldUnitIndex);
    	    
    	    value *= UNIT_VALUE_PER_INCH[newUnit]/UNIT_VALUE_PER_INCH[oldUnit];
    	    return value;
    	};
    
    //String polyfills
	
    if (typeof String.prototype.trim != 'function') {
        String.prototype.trim = function() {
            return this.replace(/^\s+/, '').replace(/\s+$/, '');
        };
    }
    
    if (typeof String.prototype.trimRight != 'function') {
        String.prototype.trimRight = function() {
            return String(this).replace(/\s+$/, '');
        };
    }
    
    if(!String.prototype.endsWith) {
    	String.prototype.endsWith = function(searchStr) {
		    var _toString = Object.prototype.toString;
		    if (_toString.call(searchStr) === '[object RegExp]') {
		    	throw new TypeError('Cannot call method "endsWith" with a regex'); //no i18n
		    }
		    searchStr = searchStr + '';
		    var thisLen = this.length;
		    var posArg = arguments.length > 1 ? arguments[1] : undefined;
		    var pos = posArg === undefined ? thisLen : 0;
		    var end = Math.min(Math.max(pos, 0), thisLen);
		    return this.slice(end - searchStr.length, end) === searchStr;
	  };
    }
    
}());
