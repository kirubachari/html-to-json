/*$Id$*/

//Array related self-executive block
(function() {
	
	//Array.prototype extensions
	
	Array.prototype.contains = function(value) {
	    for (var i = 0; i < this.length; i++) {
	        if (this[i] === value) {
	            return true;
	        }
	    }
	    return false;

	};
	
    Array.prototype.repeat = function(value, length) {
        while (length) {
            this[--length] = value;
        }
        return this;
    };
    
    Array.prototype.get = function( value, condition ){

        for( var idx=0, len=this.length; idx<len; idx++){
            if( condition( this[idx], value ) ){
                return this[idx];
            }
        }
        return null;
    };
    
    //Array polyfills
    
    if (!Array.isArray) {
        Array.isArray = function(value) {
            return value && typeof value === 'object' && Object.prototype.toString.call(value) === '[object Array]'; //No I18N
        };
    }
    
    // Production steps of ECMA-262, Edition 6, 22.1.2.1
    if (!Array.from) {
        Array.from = (function() {
            var toStr = Object.prototype.toString;
            var isCallable = function(fn) {
                return typeof fn === 'function' || toStr.call(fn) === '[object Function]'; //No I18N
            };

            var toInteger = function(value) {
                var number = Number(value);
                if (isNaN(number)) {
                    return 0;
                }
                if (number === 0 || !isFinite(number)) {
                    return number;
                }
                return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
            };

            var maxSafeInteger = Math.pow(2, 53) - 1;
            var toLength = function(value) {
                var len = toInteger(value);
                return Math.min(Math.max(len, 0), maxSafeInteger);
            };

            // The length property of the from method is 1.
            return function from(arrayLike/*, mapFn, thisArg */) {
                // 1. Let C be the this value.
                var C = this;

                // 2. Let items be ToObject(arrayLike).
                var items = Object(arrayLike);

                // 3. ReturnIfAbrupt(items).
                if (arrayLike == null) {
                    throw new TypeError('Array.from requires an array-like object - not null or undefined');//No I18N
                }

                // 4. If mapfn is undefined, then let mapping be false.
                var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
                var T;
                if (typeof mapFn !== 'undefined') {
                    // 5. else
                    // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
                    if (!isCallable(mapFn)) {
                        throw new TypeError('Array.from: when provided, the second argument must be a function');//No I18N
                    }

                    // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
                    if (arguments.length > 2) {
                        T = arguments[2];
                    }
                }

                // 10. Let lenValue be Get(items, "length").
                // 11. Let len be ToLength(lenValue).
                var len = toLength(items.length);

                // 13. If IsConstructor(C) is true, then
                // 13. a. Let A be the result of calling the [[Construct]] internal method 
                // of C with an argument list containing the single item len.
                // 14. a. Else, Let A be ArrayCreate(len).
                var A = isCallable(C) ? Object(new C(len)) : new Array(len);

                // 16. Let k be 0.
                var k = 0;
                // 17. Repeat, while k < len… (also steps a - h)
                var kValue;
                while (k < len) {
                    kValue = items[k];
                    if (mapFn) {
                        A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
                    } else {
                        A[k] = kValue;
                    }
                    k += 1;
                }
                // 18. Let putStatus be Put(A, "length", len, true).
                A.length = len;
                // 20. Return A.
                return A;
            };
        }());
    }
	
})();

//Default methods to be global
(function(){
	
	var slice = Array.prototype.slice,
	toString = Object.prototype.toString,
	hasOwnProperty = Object.prototype.hasOwnProperty;

	var each = function(obj, iterator, context) {
		if (Array.isArray(obj)) {
			for (var i = 0, l = obj.length; i < l; i++) {
				iterator.call(context, obj[i]);
			}
		} else {
			for (var key in obj) {
				if (hasOwnProperty.call(obj, key)) {
					iterator.call(context, obj[key]);
				}
			}
		}
	};

	var extend = function(obj) {
		var args = slice.call(arguments, 1);
		each(args, function(source) {
			for (var key in source) {
				if (hasOwnProperty.call(source, key)) {
					obj[key] = source[key];
				}
			}
		});

		return obj;
	};
	
	/*
     * asyncFor progress handler is sent three arguments 
     * 1 - item,
     * 2 - i
     * 3 - the array itself
     * + the remaining arguments
     */
    var asyncFor = function(context, array) {
        var args = slice.call(arguments, 2);
        return Deferred(function(dfd) {
            var sleep = isV8Engine ? 0 : 10,
                //chunkSize = 5,
                done = 0, total = array.length, i, tableOps = false;

            var processChunk = function() {
                var dfdArgs;
                if (!_stopPagination && done < total) {
                    for (i = 0; i < chunkSize && done < total; i += 1, done += 1) {
                        /** As of now Table and row nodes build will be done with chunkSize of 100 as
                          * constructing 5 nodes at a chunk is not sufficient for table nodes. */
                        //TODO Need to change to chunkSize of 5 and should handle while appending view
                        var currNode = array[done];
                        //tableOps will avoid executing this check again if chunkSize is already 100
                        if(!tableOps && (currNode.node.type === 'table' || currNode.node.type === 'row') && //No I18N
                           (currNode.op === 'create' || currNode.op === 'update')) { //No I18N
                            chunkSize = 100;
                            tableOps = true;
                        }
                        dfd.notifyWith.apply(dfd, [context, [array[done], done, array].concat(args)]);
                        //PubSub.publish("Document/RenderingState", done, total, avgState, DocTreeManager.doc.docopBuilder.operation); //No I18n
                    }
                    //Resting the chunkSize to 5 after table nodes are constructed
                    chunkSize = 5;
                    tableOps = false;
                    asyncForTimeOut = setTimeout(processChunk, sleep);
                } else {
                    dfd.resolveWith(context);
                }
            };
            dfd.start = processChunk;
        });
    };
    
    var clone = function(obj) {
        var newObj = {};
        for (var key in obj) {
            newObj[key] = obj[key];
        }
        return newObj;
    };
    
    extend(this, {
    	extend		: extend,
    	asyncFor	: asyncFor,
        clone		: clone
    });
	
})();
