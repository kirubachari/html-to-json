/* $Id:$  */
HTMLUtil = (function(){
	
	var list_type = {
		    TOC_TYPE_1          :   128,
		    NONE                :   255,
		    DECIMAL             :   0,
		    LOWER_ALPHA         :   4,
		    UPPER_ALPHA         :   3,
		    LOWER_ROMAN         :   2,
		    UPPER_ROMAN         :   1,
		    DISC                :   6,
		    CIRCLE              :   7,
		    BOX                 :   8,
		    SYMBOL              :   9,
		    IMAGE               :   10,
		    RECENT_ORDER        :   0,
		    RECENT_UNORDER      :   6,
		    CHECKLIST			:	256
		},
	
		inlineElems = [ 'a','abbr','acronym','b','bdo','big','br','cite','code',//No I18N
			            'dfn','em','i','input','kbd','label','q','samp','select',//No I18N
			            'small','span','strong','sub','sup','textarea','tt','var',//No I18N
			            '#text','img','map','object','font','aside','comment' ],//No I18N
			
			blockElems = [ "table","hr","p","div","center","li","ol","ul","blockquote",//No I18N
							"h1","h2","h3","h4","h5","h6","address","pre","dl", "td", "th", "tr" ],//No I18N
	
	findNodesInTable = function(node,nodeNametofind){    //Not considering inner table
 	    var foundNodes = new Array();
 	    var parentTable = (nodeNameEquals(node, 'table')) ? node : getParentNode( node, 'table');//no i18n
 	    node.querySelectorAll(nodeNametofind).forEach(function( ele ){
 	    	if( getParentNode( ele, 'table') == parentTable){
 	    		foundNodes.push(ele); 
 	        }
 	    });
 	    return foundNodes;
	},
	
	getComputedStylesFromBrowser = (function() { //move to zwhtmlutils.js
		if (window.getComputedStyle) {
			return function(element, properties) {
				var elementProps = getComputedStyle(element, null),
					props = {},
					prop;
				for (var idx = 0, len = properties.length; idx < len; idx++) {
					prop = properties[idx];
					props[prop] = elementProps[prop];
				}
				return props;
			};
		} else {
			return function(element, properties) {
				var props = {},
					prop;
				for (var idx = 0, len = properties.length; idx < l; idx++) {
					prop = properties[idx];
					props[prop] = element.currentStyle[prop];
				}
				return props;
			};
		}
	}()),
	
	nodeNameEquals = function(aNode,aTags){
		
		aTags = Array.isArray( aTags ) ? aTags.join( '|' ) : aTags;
		
		if (!aNode || !aTags) {
	        return false;
	    }
	    if ( aTags.indexOf( '*' ) != -1 ) {
	        return true;
	    }
	    var tags = aTags.split('|');
	    return tags.indexOf( aNode.nodeName.toLowerCase() ) != -1;
	},
	
	getListType = function( node ){
		var computedStyles = HTMLUtil.getComputedStylesFromBrowser( node , ["listStyleType"]),//No I18N
			listStyleType = computedStyles.listStyleType;
		if( listStyleType != 'none' ){

			 switch ( listStyleType ) {
		        case 'decimal': // No I18N
		            listStyleType = list_type.DECIMAL;
		            break;
		        case 'lower-alpha': // No I18N
		            listStyleType = list_type.LOWER_ALPHA;
		            break;
		        case 'upper-alpha': // No I18N
		            listStyleType = list_type.UPPER_ALPHA;
		            break;
		        case 'lower-roman': // No I18N
		            listStyleType = list_type.LOWER_ROMAN;
		            break;
		        case 'upper-roman': // No I18N
		            listStyleType = list_type.UPPER_ROMAN;
		            break;
		        case 'disc': // No I18N
		            listStyleType = list_type.DISC;
		            break;
		        case 'circle': // No I18N
		            listStyleType = list_type.CIRCLE;
		            break;
		        case 'square': // No I18N
		            listStyleType = list_type.BOX;
		            break;
		        default:
		            listStyleType = 1;
	    	}

		} else if( nodeNameEquals( node, "ol" ) ){ //NO I18N
			listStyleType = list_type.DECIMAL;
		} else {
			listStyleType = list_type.DISC;
		}
	    
	    return listStyleType;
	},
	
	getListTypeByCharCode = function( str ){
		
		var charCode = str.trim().charCodeAt( 0 ),
		listType = list_type.DECIMAL;
		
		if( charCode == 183 ){
			listType = list_type.DISC;
		} else if( charCode == 111 ){
			listType = list_type.CIRCLE;
		} else if( charCode == 167 ){
			listType = list_type.BOX;
		} else if( charCode >= 48 && charCode <= 57 ){
			listType = list_type.DECIMAL;
		} else if( charCode >= 97 && charCode <= 122 ){
			listType = list_type.LOWER_ALPHA;
		} else if( charCode >= 65 && charCode <= 90 ){
			listType = list_type.UPPER_ALPHA;
		} 
		
		return listType;
	},
	
	getParentNode = function( node, parentNodeName ){

		parentNodeName = Array.isArray( parentNodeName ) ?
			                parentNodeName.join( '|' ) :
			                parentNodeName;

		var parentNode = node.parentNode,
			resultNode = null;
		
		while( parentNode ){
			if( nodeNameEquals( parentNode, parentNodeName ) ){
			    resultNode = parentNode;
			    break;
			}
			parentNode = parentNode.parentNode;
		}
		
		return resultNode;
	},
	
	insertAfter = function (newNode, referenceNode) {
	    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
	},
	
	insertBefore = function (newNode, referenceNode) {
	    referenceNode.parentNode.insertBefore(newNode, referenceNode);
	},

	getElementWidth = function ( node ){
		var computedProps = HTMLUtil.getComputedStylesFromBrowser( node, [ "width", "borderLeftWidth", "borderRightWidth"]),//no i18n
			width = computedProps.width.convertor().pixels,
			left = computedProps.borderLeftWidth.convertor().pixels,
			right = computedProps.borderRightWidth.convertor().pixels;

		return !isNaN( width - left - right ) ? width - left - right : width;
	},
	
	_fixColSpanAlone = function(rowEl) {
        var cellElems, cellElemsLen, cellEl, jCellEl;
        var colSpan;
        var dummyCell;
        cellElems = findNodesInTable(rowEl, 'td,th'); //No I18N
        cellElemsLen = cellElems.length;
        for(var i=0; i<cellElemsLen; i++) {
            cellEl = cellElems[i];
            colSpan = parseInt(cellEl.getAttribute('colspan') || 1); //No I18N
            if(colSpan > 1) {
                for(var j=0; j<colSpan-1; j++) {
                    dummyCell = _createDummyTD('hmp'); //No I18N
                    insertAfter( dummyCell, cellEl );
                }
            }
        }
    },
    
    _getRowFixData = function(rowEl) {
        return JSON.parse(rowEl.getAttribute('data-rowfix') || '[]'); //No I18N
    },
    
    _removeRowFixData = function(rowEl) {
        rowEl.removeAttribute('data-rowfix');
    },
    
    _setRowFixData = function(rowEl, dataList) {
        var fixesList = _getRowFixData(rowEl);
        fixesList = fixesList.concat(dataList);
        rowEl.setAttribute('data-rowfix', JSON.stringify(fixesList));
    };
    
    _fixDerivedColSpanRowSpanInRow = function(rowEl) {
        var fixesList = _getRowFixData(rowEl);
        var fixesListLen = fixesList.length;
        if(fixesListLen == 0) {
            return;
        }
        _removeRowFixData(rowEl);
        //Sorting in index order
        fixesList.sort(function(a, b) {
            return a.idx - b.idx;
        });
        var cellElems, cellEl;
        var toBeFixed, idx, vmp, hmp;
        var dummyCell;
        for(var i=0; i<fixesListLen; i++) {
            toBeFixed = fixesList[i];
            idx = toBeFixed.idx;
            vmp = toBeFixed.vmp;
            hmp = toBeFixed.hmp;
            cellElems = findNodesInTable(rowEl, 'td,th'); //No I18N
            cellEl = cellElems[idx];
            //Insert vmp here
            for(var j=0; j<vmp; j++) {
                dummyCell = _createDummyTD('vmp'); //No I18N
                if(cellEl) {
                	insertBefore( dummyCell, cellEl );
                } else {
                	rowEl.appendChild( dummyCell );
                }
            }
            //Insert hmp here
            for(var k=0; k<hmp; k++) {
                dummyCell = _createDummyTD('hmp'); //No I18N
                if(cellEl) {
                	insertBefore( dummyCell, cellEl );
                } else {
                	rowEl.appendChild( dummyCell );
                }
            }
        }
    },
    
    _createDummyTD = function(mergeType) {
        var newTD = HTMLPreProcessor.getDummyNode('td');//No I18N
        var para = HTMLPreProcessor.getDummyNode('p');//No I18N
        para.appendChild(document.createElement('br'));
        newTD.appendChild(para);
        if(mergeType) {
            setAttribute( newTD, {'merge':mergeType});//No I18N
            newTD.style.display = "none";
        }
        return newTD;
    },
	
	fixMergeforTableEl = function(tableEl) {
        var rowElems = findNodesInTable(tableEl, 'tr'); //No I18N
        var rowElemsLen = rowElems.length;
        var rowEl;
        var cellElems, cellElemsLen, cellEl, jCellEl;
        var rowSpan, colSpan;
        var dummyCell;
        var data;
        for(var i=0; i<rowElemsLen; i++) {
            rowEl = rowElems[i];
            _fixColSpanAlone(rowEl);
            _fixDerivedColSpanRowSpanInRow(rowEl);
            cellElems = findNodesInTable(rowEl, 'td,th'); //No I18N
            cellElemsLen = cellElems.length;

            //var newIdx = 0;

            for(var j=0; j<cellElemsLen; j++/*,newIdx++*/) {
                cellEl = cellElems[j];
                rowSpan = parseInt(cellEl.getAttribute('rowspan') || 1); //No I18N
                colSpan = parseInt(cellEl.getAttribute('colspan') || 1); //No I18N

                if(rowSpan > 1) {
                    //Add data attribute to the affected rows to process
                    data = {};
                    data.idx = j;
                    data.vmp = 1;
                    data.hmp = colSpan - 1;

                    for(var k=1; k<rowSpan && (i+k)<rowElemsLen; k++) {
                        _setRowFixData(rowElems[i+k], [data]);
                    }
                }
                if(rowSpan > 1 || colSpan > 1) {
                	setAttribute( cellEl, {'merge':'fc'} );//no i18n
                }
            }
        }
    },
    
    getElementWidth = function ( node ){
    	var computedProps = HTMLUtil.getComputedStylesFromBrowser( node, [ "width", "borderLeftWidth", "borderRightWidth"]),//no i18n
    		width = computedProps.width.convertor().pixels,
    		left = computedProps.borderLeftWidth.convertor().pixels,
    		right = computedProps.borderRightWidth.convertor().pixels;

    	return !isNaN( width - left - right ) ? width - left - right : width;
    },
    
    setAttribute = function( node, obj ){
    	for( var key in obj ){
    		node.setAttribute( key, obj[ key ]);
    	}
    },
    
    getDisplayType = function( node ){

        if( inlineElems.indexOf( node.nodeName.toLowerCase( ) ) != -1 ){
            return "inline";//No I18N
        }

        if( blockElems.indexOf( node.nodeName.toLowerCase( ) ) != -1 ){
            return "block";//No I18N
        }


        var cStyle = "inline";//No I18N

        try {
            var ele = document.createElement( node.nodeName.toLowerCase() ),
            gcs = "getComputedStyle" in window;//No I18N

            document.body.appendChild( ele );
            cStyle = ( gcs ? window.getComputedStyle( ele, "" ) : ele.currentStyle ).display;
            document.body.removeChild( ele );
            if( cStyle == "block" ){
            	blockElems.push( node.nodeName.toLowerCase() );
            } else {
            	inlineElems.push( node.nodeName.toLowerCase() );
            }
        } catch( ex ){

        }
        return cStyle.toLowerCase();

    },
    
    isBlockElement = function( node ){
        return getDisplayType( node ) == "block";//No I18N
    },

    isInlineElement = function( node ){
        return getDisplayType( node ) == "inline";//No I18N
    },
    
    getBlockParent = function( node ){

        var parentNode = node.parentNode;

        do{
            if( parentNode && isBlockElement( parentNode )){
                break;
            }
            parentNode = parentNode.parentNode;
        }while( parentNode );

        return parentNode;

    },
    
    getParents = function( node, targetParentName ){
    	
    	var parents = [ ],
        	parentNode = node.parentNode;

        if( nodeNameEquals( node, 'body') ){
            return parents;
        }
        
        do{
            if( nodeNameEquals( parentNode, 'body' )){ //No I18n
                break;
            }
            
            nodeNameEquals( parentNode, targetParentName ) &&
            parents.push( parentNode );
            
            parentNode = parentNode.parentNode;
        }while( parentNode );
        
        return parents;
    },
    
    getDataSet = function( node ){

        //returns JSON of all data attributes starting with 'data' in that particular node

        var dataSet = node.dataset,
        dataAttributes = {},
        keys = Object.keys( dataSet ),
        len = keys.length;
        for( var idx = 0; idx < len; idx++ ){
            var dataAttr = dataSet[ keys[ idx ] ];
            dataAttributes[ keys[ idx ] ] = ZWJSONUtils.isJSON( dataAttr ) ? JSON.parse( dataAttr ) : dataAttr;
        }
        return dataAttributes;
    }
    
    getParentsUntil = function( node, nodeName ){

        //process until body only

        var parents = [ ],
        parentNode = node.parentNode;

        if( nodeNameEquals( node, 'body') ){
            return parents;
        }

        do{
            if( nodeNameEquals( parentNode, nodeName ) ||
                    nodeNameEquals( parentNode, 'body' )){ //No I18n
                break;
            }

            parents.push( parentNode );
            parentNode = parentNode.parentNode;
        }while( parentNode );

        return parents;
    };
    
    return {
    	getParents		:	getParents,
    	getParentsUntil	:	getParentsUntil,
    	getBlockParent	:	getBlockParent,
    	isInlineElement	:	isInlineElement,
    	isBlockElement	:	isBlockElement,
    	getDisplayType	:	getDisplayType,
    	setAttribute	:	setAttribute,
    	getElementWidth	:	getElementWidth,
    	fixMergeforTableEl	:	fixMergeforTableEl,
    	insertAfter		:	insertAfter,
    	insertBefore	:	insertBefore,
    	getParentNode	:	getParentNode,
    	getListTypeByCharCode	:	getListTypeByCharCode,
    	getListType		:	getListType,
    	nodeNameEquals	:	nodeNameEquals,
    	findNodesInTable:	findNodesInTable,
    	getDataSet		:	getDataSet,
    	getComputedStylesFromBrowser	:	getComputedStylesFromBrowser
    };
	
})();