/* $Id$ */
var HTMLPreProcessor = ( function(){
    "use strict"; //No I18N
    
    var _validTagsTobeProcessed = [],
    
    processedConfig = {}, defaultConfig = {
    		/*
    		 don't change the order, 
    		 whitespace should be called before table processing, 
    		 otherwise empty cell problem will occur sometimes
    		*/
    		
    		/*
    		 * Bookmark & headings moved ahead of whitespace since it will remove span with text nodes
    		 * Otherwise, It will create empty para inside cell and the para will be removed
    		 * in validnode operation 
    		 */
    		
    		para : {}, text : {}, bookmarks : {}, headings : {}, textBox : {},
    		list : {}, whiteSpace : {}, table : {}, anchor : {}, image : {},
    		iframe : {},  lineBreak : {}, horizontalRule : {},
    		noteSettings : {}, conditionMerge : {}, 
    		input : {}, comments : {}
    };
    
    var _getListFormatInfo = function( node ){
        
        var listObj = node.getAttribute( 'data-list-format' ),
        levels;
        
        if( listObj ){
            
            levels = JSON.parse( listObj );
            
        } else {
            
            var listStyleType = HTMLUtil.getListType( node ),
                startNum = node.getAttribute( 'start' ),
                children = node.querySelectorAll( 'ol,ul' );//no i18n

            levels = ZWJSONUtils.clone( LIST_STYLE_OBJECT[ listStyleType ]);

            levels.level0.startsWith = startNum ? 
                    parseInt( startNum ) : 1;
            
            for( var idx = 1; idx <= 8; idx++ ){
                var ele = children[ idx - 1 ],levelStart;
                if( ele != undefined ){
                    levelStart = ele.getAttribute( 'start' );
                    levels[ 'level' + idx ].type = HTMLUtil.getListType( ele );
                    levels[ 'level' + idx ].startsWith = levelStart ? 
                            parseInt( levelStart ) : 1;
                } else {
                    break;
                }
            }
        }
        
        return levels;
        
    },
                
    _processInputElems = function( node ){
    	typeof PubSub != "undefined" && PubSub.publish("HTMLPreProcessor/Input", [node]);//no i18n
    },
    
    _processTextBox = function( node ){
    	typeof PubSub != "undefined" && PubSub.publish("HTMLPreProcessor/TextBox", [node]);//no i18n
    },
    
    _processNoteContainers = function( node ){

        var noteSettings = {};
        typeof PubSub != "undefined" && PubSub.publish("HTMLPreProcessor/NoteContainer", [node, noteSettings]);//no i18n
    	return noteSettings;
    },
    
    _validateJSON = function( node, attrList ){
    	
    	var isInvalid = false;
    	
    	for( var attrIdx = attrList.length - 1; attrIdx > -1; attrIdx-- ){
    		
    		var elems = node.querySelectorAll( '[' + attrList[ attrIdx ] + ']' );
    		
    		for( var eleIdx = elems.length - 1; eleIdx > -1; eleIdx-- ){
    			
    			var attr = elems[ eleIdx ].getAttribute( attrList[ attrIdx ] );
    			
    			try{
    				JSON.parse( attr );
    			} catch( e ){
    				isInvalid = true;
    			}
    			
    			if( isInvalid ){
    				break;
    			}
    			
    		}
    		
    	}
    	
    	if( isInvalid ){
        	
        	for( var attrIdx = attrList.length - 1; attrIdx > -1; attrIdx-- ){
        		
        		var elems = node.querySelectorAll( '[' + attrList[ attrIdx ] + ']' );
        		
        		for( var eleIdx = elems.length - 1; eleIdx > -1; eleIdx-- ){
        			elems[ eleIdx ].removeAttribute( attrList[ attrIdx ]);
        		}
        	}
    	}
    	
    },
    
    _findListCommentNode = function( node ){
    	var childNodes = node.childNodes,
    		childArray = Array.prototype.slice.call( childNodes );	

    	for( var idx = 0, len = childArray.length; idx < len; idx++ ){
    		var item = childArray[ idx ];
    		if( item.nodeType === Node.COMMENT_NODE && item.textContent == "[if !supportLists]" ){
    			return item;
    		} else if( item.nodeType == Node.ELEMENT_NODE ){
    			var res = _findListCommentNode( item );
    			if( res ){
    				return res;
    			}
    		}
    	}
    	return null;
    },
    
    _processListElements = function( node ){
    	
    	_processOrphanListElems( node );
 
        var listFormatJSON = {};
        
        listFormatJSON.listInfo = {};
        listFormatJSON.bulletInfo = {};
        
        _validateJSON( node, [ 'data-spl-bullet-format', 'data-list-format', 'data-list-id' ]);
 
        //outside list
        var processNonWriterListElems = function(){
 
            var nodes = node.querySelectorAll( 'ol:not([data-list-id]),ul:not([data-list-id])' ),//no i18n
                len = nodes.length,
                validNodes = [ ];
 
            for( var idx = 0; idx < len; idx++ ){
                        
                /*
                 Inner ol, ul elements is not valid since they are children of
                 another ol, ul elements
                 */
                
                var ele = nodes[ idx ];
                if( !HTMLUtil.nodeNameEquals( ele.parentNode, 'ol|ul') ){
                    validNodes.push( ele );
                }
                
            }
 
            var len = validNodes.length;
 
            for( var idx = 0; idx < len; idx++ ){
                        
                var ele = validNodes[ idx ],
                newListId = Utils.getRandomNumber();
                
                //setting new Id
                ele.setAttribute( 'data-list-id', newListId );
                
                var listElems = ele.querySelectorAll( 'ol,ul,li' ),//no i18n
                    listInfo = _getListFormatInfo( ele );

                for( var _eleIdx = 0, _len = listElems.length; _eleIdx < _len; _eleIdx++ ){
                	listElems[ _eleIdx ].setAttribute( 'data-list-id', newListId );
                }
                listFormatJSON.listInfo[ newListId ] = listInfo;
            }
 
        },
 
        listHandlingForPasteFromWord = function(){
                
            //getListFormatFromWord( node ); //TODO:: Yet to fix the function
            var parentHTMLNode = HTMLUtil.getParentNode( node, 'html' ); //NO I18N
            if( !parentHTMLNode ){
                return;
            }
            var regExp = /mso-list:\s*l\d+\s+level\d+\s+lfo\d+/g, 
            innerHTMLNode = parentHTMLNode.innerHTML,
            msoListsDetected = Boolean( innerHTMLNode.match( regExp ) );/* && pasteInfo.HTML.match( /List Definitions/ )); */ //P8-3981
 
             if( msoListsDetected ){
 
                var listIdMapping = {},
                    paras = node.querySelectorAll( 'p, h1, h2, h3, h4, h5, h6' ),//NO i18n
                    listIdMapping = { };
 
                for( var idx = 0, len = paras.length; idx < len; idx++ ){
                    
                    var para = paras[ idx ],
                    styleAttr = para.getAttribute( 'style' );
                    
                    if( !styleAttr ){
                        continue;
                    }
                    
                    var resArr = styleAttr.match( regExp );
                    
                    if( resArr ){
                        
                        var propValue = resArr[ 0 ],
                            level = propValue.match( /level(.+?)\s+/ )[ 1 ],
                            id = propValue.match( /l(\d+?)\s+/ )[ 1 ],
                            listType,
                            typeFound = false;
                        
                        var childNodes = Array.from( para.childNodes ),
                        childIdx = 0,
                        childLen = childNodes.length;
                        
                        var _tgtCommentNode = _findListCommentNode( para );
                        if( _tgtCommentNode ){
                        	var _listNumberNode = _tgtCommentNode.nextSibling,
                        		textContent = _listNumberNode.textContent,
                        		listType = HTMLUtil.getListTypeByCharCode( textContent );
                           
                        	typeFound = true;
                        	
                        	_listNumberNode.parentNode.removeChild( _listNumberNode );
                        }
                        
                        if( !listIdMapping.hasOwnProperty( id ) ){
                            listIdMapping[ id ] = Utils.getRandomNumber();
                            listFormatJSON.listInfo[ listIdMapping[ id ] ] = listType;
                        }
                        
                        /* -  P8-6224
                         * after removing number span, Para may be empty.
                         * So, para element will be removed if there is no text node inside
                         */
                        
                        if( para.innerText.length == 0 ){
                        	var newSpan = document.createElement( 'span' );
                    		
                        	newSpan.classList.add( "EOP" );//No I18N
                            newSpan.innerHTML = "&nbsp;"; //No I18N
                            para.appendChild(newSpan);
                        }
                        
                        HTMLUtil.setAttribute( para, {
                            'data-list-info'    :    JSON.stringify( //No I18N 
                                                        { 'id' : listIdMapping[ id ], 
                                                        'l' :    parseInt( level ) - 1 }),//No I18N
                            'class'                :    'ms-list'//No I18N
                        });
                        
                    }
                    
                }
                
            }
        };
        
        typeof PubSub != "undefined" && PubSub.publish( "HTMLPreProcessor/List", [ node, listFormatJSON ]);//no i18n
         //processWriterListElems();
 
        processNonWriterListElems();
 
        listHandlingForPasteFromWord();
        
        !ZWJSONUtils.isEmptyObject( listFormatJSON.bulletInfo ) ? listFormatJSON.bulletInfo.st = 'bulletStyles' : undefined; //NO I18N
        !ZWJSONUtils.isEmptyObject( listFormatJSON.listInfo ) ? listFormatJSON.listInfo.st = 'listFormat' : undefined; //NO I18N
 
         return listFormatJSON;
    },
    
    _processOrphanListElems = function( node ){
        
    	var liElems = node.querySelectorAll( 'li' ),//no i18n
        	_validArr = [];
        
    	for( var _i = 0, _len = liElems.length; _i < _len; _i++ ){
    		if( !HTMLUtil.nodeNameEquals( liElems[ _i ].parentNode, 'ul|ol' ) ){
    			_validArr.push( liElems[ _i ] );
    		}
    	}
    	
        /*
         'li' element which is not children of 'ol' or 'ul'
         append to ul
         
         */
    	
    	for( var j = 0, _len = _validArr.length; j < _len; j++ ){
    		
    		var _listBlock = document.createElement("ul"),
				_firstLi = _validArr[ j ];
	    	
    		_firstLi.parentNode.insertBefore( _listBlock, _firstLi );
    		
    		var _temp = [ _firstLi ];
    		
    		while( HTMLUtil.nodeNameEquals( _firstLi.nextSibling, 'ol|ul|li' )){//no i18n
    			if( HTMLUtil.nodeNameEquals( _firstLi.nextSibling, 'li' )){ //could be sublevel
    				j += 1;
    			}
    			_temp.push(  _firstLi.nextSibling );
    			_firstLi = _firstLi.nextSibling;
    		}
    		
    		
    		for (var _len1 = _temp.length - 1; _len1 >=0; --_len1 ){
    			_listBlock.insertBefore(_temp[_len1], _listBlock.firstChild);
    		}
    		
    	}
        
    },
    
    _processAnchors = function( node ){
        var anchors = node.querySelectorAll( 'a' );//no i18n
        
        
        for( var idx = 0, len = anchors.length; idx < len; idx++ ){
            var anchor = anchors[ idx ];
            anchor.setAttribute( 'data-link-info', JSON.stringify( CSSToJSON.getLinkStyles( anchor ) ) );
            anchor.setAttribute( 'id', Utils.randomString() );
        }
        
    },
    
    _processHeadings = function( node ){
    	
    	typeof PubSub != "undefined" && PubSub.publish( "HTMLPreProcessor/Headings", [ node ]);//no i18n
    	
    },
    
    _processComments = function( node ){
    	
        typeof PubSub != "undefined" && PubSub.publish( "HTMLPreProcessor/Comments", [ node ]);//no i18n

    },
    
    _processBookmarks = function( node ){

        typeof PubSub != "undefined" && PubSub.publish( "HTMLPreProcessor/Bookmarks", [ node ]);//no i18n
    	    	
    },
    
    //cols attribute
    _getColsForTableNode = function( tableEle ){
        
        var rowIdx = 0,
        hasColSpanAttr = function( td ){ 
            return td.hasAttribute( 'colspan' ) && parseInt( td.getAttribute( 'colspan' ) ) > 1; //No I18n
        },
        isNotValidCell = function( td ){
            return hasColSpanAttr( td ) || ( td.hasAttribute( 'merge' ) && td.hasAttribute( 'merge' ) != "fc" ); //No I18n
        },
        rowElems = HTMLUtil.findNodesInTable( tableEle, 'tr' ),//no i18n
        row_len = rowElems.length,
        isValidRowFound = true;
        
        for( var idx = 0; idx < row_len; idx++ ){
            var rowEle = rowElems[ idx ],
            tds = HTMLUtil.findNodesInTable( rowEle, 'td,th' ); //No I18N
            
            if( tds.length == 0 ){
                continue;
            }
            
            isValidRowFound = true;
            rowIdx = idx;
            for( var tdsIdx = 0, tdsLength = tds.length; tdsIdx < tdsLength; tdsIdx++ ){
            	if( isNotValidCell( tds[ tdsIdx] ) ){
                    isValidRowFound = false;
                    break;
            	}
            }
            
            if( isValidRowFound ){
                break;
            }
            
        }
        
        var cellElems = HTMLUtil.findNodesInTable( rowElems[ rowIdx ], 'td,th' ), //No I18N
            cols = [],
            tableWidth = HTMLUtil.getElementWidth( tableEle ); 
        
        if( isValidRowFound ){
        	
        	cellElems.forEach(function( cellEle, idx ){
        		var hasCspan = hasColSpanAttr( cellEle ),
                cspan = 1;
                
                if( hasCspan ){
                    cspan = parseInt( cellEle.getAttribute( 'colspan' ) );
                }
                
                var col,
                cellWidth = HTMLUtil.getElementWidth( cellEle );
                cellWidth = ( cellWidth / cspan ) + UNITS.PIXELS;
                        
                for( var cIdx = 0; cIdx < cspan; cIdx++ ){
                    cols.push( cellWidth );
                }
        	});
        } else {
            
            var noOfCell = cellElems.length,
            cellWidth = ( tableWidth / noOfCell ).toFixed( 2 );
            cellWidth = cellWidth + UNITS.PIXELS;
            
            for( var cellIdx = 0, len = noOfCell; cellIdx < len; cellIdx++ ){
                cols.push( cellWidth );
            }
            
        }
        
        return cols;
    },
 
    _hasVaildImage = function( node ){
    	var imgs = node.querySelectorAll('img');//no i18n
    	
    	for( var idx = 0, len = imgs.length; idx < len; idx++ ){
    		var img = imgs[ idx ];
    		if( img.hasAttribute( 'src' ) && img.getAttribute('src' ) != null ){
    			return true;
    		}
    	}
    	
    	return false;
    },
    
    _processTD = function( cellElem, isTablePaste ){
        
        var childNodes = cellElem.childNodes,
        	len = childNodes.length;
        
        if( _hasVaildImage( cellElem ) ){
        	return false;
        } else if( ( !cellElem.textContent.length || !len || cellElem.innerText == UNICODE.TAB ) && // P8-6359, P8-4776 both the check are required before creating dummy para for empty cell
        	cellElem.querySelector('br') == null ){  //no i18n
            
            cellElem.appendChild( _getDummyNode( "p" ).appendChild( document.createElement( "br" ))); //No I18N
            return;
            
        } else if( isTablePaste){ //table to table paste case if in case of text only content
        	//It doesn't go through normal flow hence added this
        	cellElem.innerHTML = "<p>" + cellElem.innerHTML + "</p>";//no i18n
        }
        
    },
    
    _fixTableForLesserCellElems = function( tableEle ){
        
        /*
         * 1. Row/Col Span Fix
         * 2. if total no of cells in row less than allowed no of cells in a row element 
         * 3. Inline element fix for table
         */
        
        var rowElems = HTMLUtil.findNodesInTable( tableEle, 'tr' ), //No I18N
        row_len = rowElems.length;
        
        //_processRowSpanColSpan(tableEle);
        HTMLUtil.fixMergeforTableEl(tableEle);

        var maxColumnCount = _getTableColumnCount( tableEle );
        
        for( var rowIdx = 0; rowIdx < row_len; rowIdx++ ){
            
            var row = rowElems[ rowIdx ],
            cellElems = HTMLUtil.findNodesInTable( rowElems[ rowIdx ], 'td,th' ), //No I18N
            tdsLen = cellElems.length;
            
            if( tdsLen == 0 ){
                row.parentNode.removeChild( row );
            }
            
            for( var cellIdx = 0; cellIdx < tdsLen; cellIdx++ ){
                
                var currTd = cellElems[ cellIdx ],
                isLastCell= ( cellIdx == tdsLen - 1 )? true: false;//P8-2385 by Kiruba
               
                _processTD( currTd );
                
            }//cell loop
            
            if( isLastCell ){
                //Shyamala added issue WR-3261 
                var tdCount = rowElems[ rowIdx ].cells.length;
                while( tdCount < maxColumnCount ){
                    var td = _getDummyNode( 'td' ),    //No I18N
                    para = _getDummyNode( 'p' );//No I18N
                    
                    para.appendChild( document.createElement( 'br' ));
                    td.appendChild( para );
                    row.appendChild( td ); 
                    tdCount++;
                }
            }
        }
        
    },
    
    _setMatrixPosition = function( tableEle ){
    	var rowElems = HTMLUtil.findNodesInTable( tableEle, 'tr' ), //No I18N
    	row_len = rowElems.length, rowCount = row_len, colCount;
    	
    	for( var rowIdx = 0; rowIdx < row_len; rowIdx++ ){
            
    		var row = rowElems[ rowIdx ],
            cellElems = HTMLUtil.findNodesInTable( rowElems[ rowIdx ], 'td,th' ), //No I18N
            tdsLen = cellElems.length;
    		typeof colCount == "undefined" ? colCount = tdsLen : undefined; //NO I18N
            for( var cellIdx = 0; cellIdx < tdsLen; cellIdx++ ){
            	var currTd = cellElems[ cellIdx ];
            	currTd.setAttribute( 'data-matrix-position', JSON.stringify( [ rowIdx, cellIdx ] ) );
            }
    	}
    	tableEle.setAttribute( 'data-children-count', JSON.stringify( { rows : rowCount,cols : colCount } ) );    	
    },
    
    _getTableColumnCount = function( table ){
        
        var columnCount = 0,
        rowElems = HTMLUtil.findNodesInTable( table, 'tr' ), //No I18N
        row_len = rowElems.length;
        
        for( var rowIdx = 0; rowIdx < row_len; rowIdx++ ){    //Issur Id :: WR-3698  
            
            var cellCount = 0,
            cellElems = rowElems[ rowIdx ].childNodes;
            
            for( var cellIdx = 0, cell_len = cellElems.length; cellIdx < cell_len; cellIdx++ ){
               
                var cellElem = cellElems[ cellIdx ]; 
               
               if( HTMLUtil.nodeNameEquals( cellElem, 'td|th' )){ //No I18N
                   cellCount += 1;
               }
            }
            
            columnCount = Math.max( columnCount, cellCount );
         }   
 
        return columnCount;
 
    },
    
    _fixTableForSiblingProblem = function( node ){
        
        var _fixTableForNextSib = function( table ){
            
            var parentNode = table.parentNode;
            
            if( HTMLUtil.nodeNameEquals( parentNode, "body" )){
                
                var _nextSib = table.nextSibling;
                if( _nextSib && HTMLUtil.nodeNameEquals( _nextSib, "table" )){
                    var para = _getDummyNode( "p" );//No I18N
                    HTMLUtil.insertAfter( para, table );
                }
            } else if( HTMLUtil.nodeNameEquals( parentNode, "td|th" )){//No I18N
                
                var _nextSib = table.nextSibling;
                if( !_nextSib || ( _nextSib && HTMLUtil.nodeNameEquals( _nextSib, "table" ))){
                    var para = _getDummyNode( "p" );//No I18N
                    HTMLUtil.insertAfter( para, table );
                }
            }
            
        },
        
        _fixTableForPrevSib = function( table ){
            
            var parentNode = table.parentNode;
            
            if( HTMLUtil.nodeNameEquals( parentNode, "body" )){
                
                var _prevSib = table.previousSibling;
                if( !_prevSib ){
                    var para = _getDummyNode( "p" );//No I18N
                    HTMLUtil.setAttribute( para, {'data-para-info' : JSON.stringify( { 'type' : NODE_TYPE.PARAGRAPH }) });
                    table.parentNode.insertBefore( para, table );
                }
            } else if( HTMLUtil.nodeNameEquals( parentNode, "td|th" )){//No I18N
                
                var _prevSib = table.previousSibling;
                if( !_prevSib ){
                    var para = _getDummyNode( "p" );//No I18N
                    table.parentNode.insertBefore( para, table );
                }
            }
            
        },
        
        tables = node.querySelectorAll( 'table' );//no i18n
        
        for( var idx = 0, len = tables.length; idx < len; idx++ ){
            _fixTableForNextSib( tables[ idx ]);
        }
        
        for( var idx = 0, len = tables.length; idx < len; idx++ ){
            _fixTableForPrevSib( tables[ idx ]);
        }
        
    },
 
    _processTableNodes = function( node ){
    	
    	_processOrphanRowCellElems( node );
        
        var tableElems = node.querySelectorAll( 'table' ),table_attr = {},//no i18n
        len = tableElems.length,
        
        _processTableNode = function( tableEle ){
            
            tableEle.setAttribute( 'id', 'tb-' + Utils.randomString() );
            
            var caption = HTMLUtil.findNodesInTable( tableEle, 'caption' ); //No I18N
            caption.forEach(function(ele){
                ele.parentNode.removeNode( ele );
            });
            
            var tableWidth,
                cols = [],
                dataWidth = tableEle.getAttribute( 'data-width' ),
                dataCols = tableEle.getAttribute( 'data-cols' );
                _fixTableForLesserCellElems( tableEle );
    		    _setMatrixPosition( tableEle );

            dataCols = (dataCols && JSON.parse( dataCols )) || [];

            var maxColumnCount = _getTableColumnCount( tableEle ),
                isValidCols = (dataCols.length == maxColumnCount);

            if(isValidCols){
                for( var idx = 0; idx < dataCols.length; idx++ ){
                    cols.push( dataCols[ idx ].wt );
                }
            } else {
                /*
                * getcols should be called after fixing table for lesser cell ELems
                * since extra cell can be added, which should be updated in cols
                */
                cols = _getColsForTableNode( tableEle );
            }
            
            if( dataWidth ){
                tableWidth = dataWidth;
                
            } else {
                tableWidth = 0;
                for( var idx = 0; idx < cols.length; idx++ ){
                    tableWidth += parseFloat( cols[ idx ] ); 
                }
                tableWidth = tableWidth + UNITS.PIXELS;
            }
            
            table_attr = {
        	    'data-width' : tableWidth, //NO I18N
                    'data-cols' : JSON.stringify( cols ), //NO I18N
            };
            HTMLUtil.setAttribute( tableEle, table_attr );
            
        };
        
        for( var idx = 0; idx < len; idx++ ){
            var tableElem = tableElems[ idx ];
            
            _processTableNode( tableElem );
        }
        
    }, 
    
    _getDummyNode = function( nodeName ){
        
        var node = document.createElement( nodeName );
        
        if( nodeName === "p" ){
            HTMLUtil.setAttribute( node, { 'data-para-info' :  JSON.stringify( { "type" : NODE_TYPE.PARAGRAPH })});
            
        } else if( nodeName === "tr" ){//No I18N
            
            node.setAttribute( 'id', 'rw-' + Utils.randomString() );
            var cellElem = document.createElement( 'td' );
            cellElem.setAttribute( 'id', 'cl-' + Utils.randomString() );
            node.appendChild( cellElem );
            
        } else if( nodeName === "td" ){//No I18N
        
            node.setAttribute( 'id', 'cl-' + Utils.randomString() );
            
        } else if( nodeName === "table" ){//No I18N
            
            node.setAttribute( 'id', 'tb-' + Utils.randomString() );
            
        }
        
        return node;
        
    },
    
    _processOrphanRowCellElems = function( node ){
        
        
        //Cell Processing
        var cellElems = node.querySelectorAll( 'td,th' ),//no i18n
        len = cellElems.length;
        
        for( var idx = 0; idx < len; idx++ ){
            var cellElem = cellElems[ idx ];
            cellElem.setAttribute( 'id', 'cl-' + Utils.randomString() );
            
            if( !HTMLUtil.nodeNameEquals( cellElem.parentNode, "tr" ) ){
                
                var table = _getDummyNode( "table" ),//No I18N
                row = _getDummyNode( "tr" );//No I18N
                
                table.appendChild( row );
                HTMLUtil.insertAfter( cellElem, table );
                row.appendChild( cellElem );
                
            }
            
        }
        
        
        //Row Processing
        var rowElems = node.querySelectorAll( 'tr' ),//no i18n
            len = rowElems.length;
        
        for( var idx = 0; idx < len; idx++ ){
            var rowElem = rowElems[ idx ];
            rowElem.setAttribute( 'id', 'rw-' + Utils.randomString( ) );
            
            //For empty rows empty cells are added, commenting it, have to fix this after rspan/cspan fixed in table
            //if( !rowElem.childNodes.length ){
            //    
            //    var cellElem = _getDummyNode( "td" );//No I18N
            //    rowElem.appendChild( cellElem );
            //    
            //}
            
            if( !HTMLUtil.nodeNameEquals( rowElem.parentNode, "table|tbody|thead|tfoot" ) ){
                var table = _getDummyNode( "table" );//No I18N
                
                HTMLUtil.insertAfter( rowElem, table );
                table.appendChild( rowElem );
            }
            
        }
        
    },
    
    _isValidTextNode = function( node ){
        return node.textContent && node.textContent.length;
    },
    
    _whiteSpacePropertyAndTextNodeHandling = function( node ){
        
        //https://developer.mozilla.org/en-US/docs/Web/CSS/white-space
        /* To handel \n and \t */
        
		if( node.nodeType == Node.ELEMENT_NODE && !node.hasAttribute( 'data-writer-tab' ) ){
            
            var styles = ["whiteSpace"];
            var whiteSpaceProp = HTMLUtil.getComputedStylesFromBrowser( node, styles ).whiteSpace,
            cont = node.innerHTML,
            isWhiteSpace = "false";
            
            if( whiteSpaceProp.indexOf( 'pre' ) != -1 ){ 
                
                isWhiteSpace = "true";
                if( ( cont.match( /\n/g ) || cont.match( /\t/g )) && cont.indexOf( 'data-text-info' ) == -1 ){
                
                    cont = cont.replace( /[\n]/g, '<br>' ); //No I18N
                    var span = document.createElement( 'span' ),
                    dataTextinfo = {
                                        "st"    :    NODE_TYPE.TAB,//NO I18N
                                        "type"    :    NODE_TYPE.TEXT,//NO I18N
                                        "leader":    "0"//NO I18N
                                    };
                    span.innerText = '\t';//NO I18N
                    span.setAttribute( 'data-text-info', JSON.stringify( dataTextinfo ));
					span.setAttribute( 'data-writer-tab', JSON.stringify({ "cls": "Apple-tab-span" }) );//NO I18N
                    cont = cont.replace( /[\t]/g, span.outerHTML );
                    node.innerHTML = cont;
                }
            }
            
            node.setAttribute( 'data-white-space', isWhiteSpace );
            
		} else if( node.nodeType == Node.TEXT_NODE && 
				!node.parentNode.hasAttribute( 'data-writer-tab' ) &&//NO I18N
				!node.parentNode.classList.contains( 'EOP' )){ //NO I18N
            
            var textContent = node.textContent;
            
            textContent = textContent.replace( / *(\n+) */g, ' ' );
            textContent = textContent.replace( / *(\t+) */g, ' ' );
            
            if( textContent.length ){
            
				var whiteSpacePropParent = node.parentNode.getAttribute( 'data-white-space');
                
                if( whiteSpacePropParent && whiteSpacePropParent == "false" ){
                    var newCont = textContent.replace( / */g, '' );//P8-3981 
                    textContent = newCont.length ? textContent : newCont;
                }
            } 
            
            var obj = { 'content' : textContent };//no i18n
            typeof PubSub != "undefined" && PubSub.publish( "HTMLPreProcessor/TextContent", obj );//no i18n
            node.textContent = obj.content;
            
        }
        
        for( var idx = 0, childNodes = node.childNodes,
                len = node.childNodes.length; idx < len; idx++ ){
            
            var childNode = childNodes[ idx ];
                _whiteSpacePropertyAndTextNodeHandling( childNode );
        }
        
    },
    
    _getValidNodes = function( node ){
    
        var _validNodes = [];
        
        for( var idx = 0, childNodes = node.childNodes,
                len = node.childNodes.length; idx < len; idx++ ){
            
            var childNode = childNodes[ idx ];
            
            if( childNode.nodeType == Node.COMMENT_NODE || 
                    HTMLUtil.nodeNameEquals( childNode, "style" )){ //NO I18N
                /*if( childNode.nodeValue == "EndFragment" ){ //NO I18N // TODO :: MSWORD list paste handling
                    break;
                } else{*/
                    continue;
                //}
                
            }
            
            if( _validTagsTobeProcessed.indexOf( childNode.nodeName.toLowerCase( ) ) != -1 ){
                
                if( childNode.nodeType == Node.TEXT_NODE ){ //Text Node
                    
                    _isValidTextNode( childNode ) ? _validNodes.push( childNode ) : ""; 
                    
                } else { //other Nodes :: as of now hr , img & br
                    
                    _validNodes.push( childNode );
                }
                
			} else if( childNode.nodeType != Node.TEXT_NODE && childNode.hasAttribute( 'data-writer-tab' ) ){ //No I18N
                
                _validNodes.push( childNode );
                
            } else {
                
                _validNodes = _validNodes.concat( _getValidNodes( childNode ));
                
            }
        }
        
        return _validNodes;
        
    },
    
    _isInsideAnchor = function( node ){
    	
        var anchor = HTMLUtil.getParentNode( node, "a" );//No I18N
        
        return anchor && anchor.hasAttribute( 'href' ) ? true : false;//No I18N
        
    },
    
    _processImgElems = function( node ){ 
    
        var imgElems = node.querySelectorAll( 'img' );//no i18n
        
        for( var idx = 0, len = imgElems.length; idx < len; idx++ ){
            
            var img = imgElems[ idx ];
            
            HTMLUtil.setAttribute(img, { "data-img-info" :  //NO I18N
                        JSON.stringify( CSSToJSON.getImageStyles( img ) ) });
        }
            
    },
    
    _processIframeElems = function( node ){
    	
    	typeof PubSub != "undefined" && PubSub.publish( "HTMLPreProcessor/Iframe", [ node ] );//no i18n
        
        var iframeElems = node.querySelectorAll( 'iframe' );//no i18n
        
        for( var idx = 0, len = iframeElems.length; idx < len; idx++ ){
            
            var iframe = iframeElems[ idx ];
            
            HTMLUtil.setAttribute(iframe, { 'data-iframe-info' : JSON.stringify( CSSToJSON.getIframeStyles( iframe ))});
        }
    },
    
    _processHr = function( node ){
        
        var hrElems = node.querySelectorAll( 'hr' );//no i18n
        
        for( var idx = 0, len = hrElems.length; idx < len; idx++ ){
            
            HTMLUtil.setAttribute( hrElems[ idx ], {'data-hr-info': JSON.stringify( CSSToJSON.getHRStyles( hrElems[ idx ] ))});
            
        }
        
    },
    
    _processUnsupportedAndNotAllowedElements = function( node ){
    	var notAllowedArr = processedConfig.notAllowedTags;
    	_processDeprecatedTags( node );
    	/*var tagsToBeRemoved = "input"; //NO I18N
    	for( var idx = 0; idx < notAllowedArr.length; idx++ ){
    		tagsToBeRemoved += "," + notAllowedArr[ idx ];
    	}
    	var elements = Array.from(node.querySelectorAll( tagsToBeRemoved )); //NO I18N
    	for( var idx = 0; idx < elements.length; idx++ ){
    		elements[ idx ].parentNode.removeChild( elements[ idx ] );
    	}*/
    },
    
    _processConditionalMerge = function( dummyNode ){
    	
    	typeof PubSub != "undefined" && PubSub.publish( "HTMLPreProcessor/MailMerge", [ dummyNode ] );//no i18n
    	
    },
    
    _processDeprecatedTags = function( node ){
    	var deprecatedTags = node.querySelectorAll( "xmp" );//no i18n
    	for( var idx = 0; idx < deprecatedTags.length; idx++ ){
    		var newElem = document.createElement( "div" ),
    		  style = deprecatedTags[ idx ].getAttribute( "style" ),
              parentNode = deprecatedTags[ idx ].parentNode;
    		
    		newElem.innerText = deprecatedTags[ idx ].innerText;
    		newElem.setAttribute( "style", style );

            parentNode.replaceChild( newElem, deprecatedTags[ idx ]);
    	}
    },
    
    _processListAsNotAllowed = function( node ){ 
		var dataAttrInList = [ "data-hlist-type", "data-continue-list-id", "data-continue-bullet-id" ], searchStr = ""; //NO I18N
		
		for( var idx = dataAttrInList.length -1; idx > -1; idx-- ){
    		searchStr += "[" + dataAttrInList[ idx] + "]";
    		if( idx != 0 ){
    			searchStr += ",";
    		}
    	}
		var result = Array.from( node.querySelectorAll( searchStr ));	
		for( var idx = result.length - 1; idx > -1; idx-- ){
			clearAttributes( result[ idx ], dataAttrInList );	
		}
	}, 
	
	_processShapeAsNotAllowed = function( node ){
		
		var result = Array.from( node.querySelectorAll( '[data-shapedata]' ));//NO I18N
		
		for( var idx = result.length - 1; idx > -1; idx-- ){
			var resultNode = result[ idx ];
			
			resultNode.parentNode.removeChild( resultNode );	
		}
	},
    
    clearAttributes = function( node, arr ){
    	for( var idx = arr.length - 1; idx > -1; idx--  ){
    		node.removeAttribute( arr[ idx ]);
    	}
    },
    
    getAllowedTagsToBeProcessed = function(){
    	var validTagsToBeProcessed = [];
    	
    	processedConfig.isAllowed.text ? validTagsToBeProcessed.push( "#text" ) : undefined ;
		
		processedConfig.isAllowed.lineBreak ? validTagsToBeProcessed.push( "br" ) : undefined;
		
		processedConfig.isAllowed.horizontalRule ? validTagsToBeProcessed.push( "hr" ) : undefined;
		
		processedConfig.isAllowed.image ? validTagsToBeProcessed.push( "img" ) : undefined;
		
		processedConfig.isAllowed.iframe ? validTagsToBeProcessed.push( "iframe" ) : undefined;
		
		return validTagsToBeProcessed;
    },
    
    getProcessedConfig = function(pasteConfig){
    	pasteConfig = pasteConfig ? pasteConfig : {};
    	var config = ZWJSONUtils.clone( defaultConfig );
    	
    	ZWJSONUtils.mergeJsonObjs( config, pasteConfig );
    	
    	var configTagMap = { 
    			"horizontalRule" : "hr",  //NO I18N
    			"lineBreak" : "br",  //NO I18N
    			"image" : "img",  //NO I18N
    			"iframe": "iframe", //NO I18N
    			"input"	:	"input", //NO I18N
    			"table"	:	"table",//NO I18N
    			"shape"	:	"shape"}; //NO I18N
	    	
    	processedConfig.isAllowed = {};
    	processedConfig.notAllowedTags = [];
    	processedConfig.fn = {};
    	
    	for( var key in config ){
    		processedConfig.isAllowed[ key ] = typeof config[ key ].isAllowed === "undefined" || config[ key ].isAllowed ? true : false; //NO I18N
    		
    		if(processedConfig.isAllowed[ key ]){
    			processedConfig.fn[ key ] = _processingAllowedFunction[ key ] ;
    		} else {
    			processedConfig.fn[ key ] = _processingUnallowedFunction[ key ];
    			configTagMap.hasOwnProperty( key ) && processedConfig.notAllowedTags.push( configTagMap[ key ] );
    		}
    	}
    	
    	//set Valid Nodes To be processed based on the config::
    	_validTagsTobeProcessed = getAllowedTagsToBeProcessed();
    	
    	processedConfig.allowedPostionToInsert = processedConfig.isAllowed.table ? [ "td", "th", "body" ] : [ "body" ]; //NO I18N
    	
    	return processedConfig;
    },
    
    _processAllElements = function( node ){
    	var processedObj = {};
    	for( var key in processedConfig.fn ){
    		processedObj[ key ] = typeof processedConfig.fn[ key ] === "function" ? processedConfig.fn[ key ]( node ) : undefined;
    	}
    	return processedObj;
    },
    
    createStructure = function( node, pasteConfig ){
    	
        var dummyNode = node, _validNodes = [], outputEle = document.createElement( 'body' );
        
        processedConfig = getProcessedConfig( pasteConfig );
        
        _processUnsupportedAndNotAllowedElements( dummyNode );
        
        var processedElementObj = _processAllElements( dummyNode );
             
        _validNodes = _getValidNodes( dummyNode );
        
        
        if( ( processedElementObj.list &&  processedElementObj.list.listInfo && 
            Object.keys( processedElementObj.list.listInfo ).length ) ){
        	var listFormats = processedElementObj.list || {};
        	
            var listInfo = listFormats.listInfo;
            
            outputEle.setAttribute( 'data-list-info', 
                                    JSON.stringify( listInfo ) );
            
            if( listFormats.hasOwnProperty( 'paraStyles') && Object.keys( listFormats.paraStyles ).length ){
                
                outputEle.setAttribute( 'data-para-info', 
                        JSON.stringify( listFormats.paraStyles ));
                
            }
        
            if( listFormats.hasOwnProperty( 'bulletInfo') && Object.keys( listFormats.bulletInfo ).length ){
                var bulletInfo = listFormats.bulletInfo;
                outputEle.setAttribute( 'data-bullet-info', 
                        JSON.stringify( bulletInfo ));
            }
            
        }
        
        if( processedElementObj.noteSettings && processedElementObj.noteSettings.footnote ){
            outputEle.setAttribute( 'data-footnote-info' ,
                                        JSON.stringify( processedElementObj.noteSettings.footnote ) );
        }
        if( processedElementObj.noteSettings && processedElementObj.noteSettings.endnote ){
            outputEle.setAttribute( 'data-endnote-info' ,
                                        JSON.stringify( processedElementObj.noteSettings.endnote ) );
        }
        
        var len =  _validNodes.length,
        idx = 0,
        textStyles;
        
        while( idx < len ){
            
            var childNode = _validNodes[ idx ],
            childArr = [ childNode ],
            parentBlock = HTMLUtil.getBlockParent( childNode ); 
            
            var para = document.createElement( "p" );
            
            //if parentBlock is td,th or body insert dummy para
            var paraStyles, textStyleOfPara;
            if( HTMLUtil.nodeNameEquals( parentBlock, "body") ){
                paraStyles = { "type"    :    NODE_TYPE.PARAGRAPH };//No I18N
            } else if( HTMLUtil.nodeNameEquals( parentBlock, "td|th") ){//No I18N
            	
                paraStyles = CSSToJSON.getParaStyleFromCell(parentBlock);//No I18N
                
            } else if( parentBlock.hasAttribute( "data-para-info" )){//No I18N
                paraStyles = JSON.parse( parentBlock.getAttribute( "data-para-info" ));
            } else{
                paraStyles = CSSToJSON.getParagraphStylesOfBlockEle( parentBlock );
            }
            
            textStyleOfPara = CSSToJSON.getTextStylesOfBlockEle( parentBlock );
            
            if( !paraStyles.hasOwnProperty( "ts" )){//no i18n
            	paraStyles.ts = {};//no i18n
            }
            
            typeof PubSub != "undefined" && PubSub.publish("HTMLPreProcessor/mergeObjs", [paraStyles.ts, textStyleOfPara]);//no i18n 
 
            if( parentBlock.hasAttribute( 'data-hlist-type' ) ){
                para.setAttribute( 'data-hlist-type', parentBlock.getAttribute('data-hlist-type') );
            }
        
            if( parentBlock.hasAttribute( 'data-continue-list-id' )){
                var continueListId = parentBlock.getAttribute( 'data-continue-list-id' );
                para.setAttribute( 'data-continue-list-id', continueListId );
            }
            
            if( parentBlock.hasAttribute( 'data-continue-bullet-id' )){
                var continueBulletId = parentBlock.getAttribute( 'data-continue-bullet-id' );
                para.setAttribute( 'data-continue-bullet-id', continueBulletId );
            }
            
            if( parentBlock.hasAttribute( 'data-writer-border-info' )){
                para.setAttribute( 'data-writer-border-info', parentBlock.getAttribute('data-writer-border-info') );
            }
            
            HTMLUtil.setAttribute( para, {
                'data-para-info' : JSON.stringify( paraStyles ) //NO I18N
            });
            
            //grouping the valid elements
            
            /*hr element should be first element of block element otherwise nodes before hr element
            won't be rendered*/
            
            while( idx + 1 < len && !HTMLUtil.nodeNameEquals( _validNodes[ idx + 1 ], 'hr'  ) && 
                    parentBlock == HTMLUtil.getBlockParent( _validNodes[ idx + 1 ] ) ){
                childArr.push( _validNodes[ idx + 1 ] );
                idx++;
            }
            
            /*
             * Inline elements those who are all direct children of body and comes after 
             * block elements should be append to body instead of para 
             */
            
            var dontPush = false;
            if( idx + 1 == len &&
                    HTMLUtil.nodeNameEquals( HTMLUtil.getBlockParent( childArr[ 0 ] ), 'body' )){
                dontPush = true;
            }
            
            var anchor, ele, 
            whereToInsert = HTMLUtil.getParentNode( childNode, processedConfig.allowedPostionToInsert );//No I18N
            
            for( var eleIdx = 0; eleIdx < childArr.length; eleIdx++ ){
            
                var _node = childArr[ eleIdx ],
                isWithTab = false;
                ele = _node;
                
                if( ele.parentNode && ele.parentNode.classList.contains( 'EOP' ) ){
                    continue;
                }
                try{
					isWithTab = _node.hasAttribute( 'data-writer-tab' )  ? true : false;//NO I18N
                }
                catch( ex ){
                    isWithTab = false;
                }
                if( ele.nodeType != Node.TEXT_NODE ){
                    
                    ele = ele.cloneNode( isWithTab );
                    
                } else {
                    
                    var textStyles = CSSToJSON.getTextStylesOfTextNode( ele ),
                    //P8-4008 copy/paste of link in IE browser within writer
                    parentSpanNode = HTMLUtil.getParentNode( ele, 'span'), //NO I18N
                    span = parentSpanNode ? parentSpanNode.cloneNode() : document.createElement( "span" );
                    if( span.hasAttribute( "data-text-info" ) ){
                        extend( textStyles, JSON.parse( span.getAttribute( "data-text-info" )) );
                    }
                    
                    span.innerText = ele.textContent;
                    span.setAttribute( 'data-text-info', JSON.stringify( textStyles ) );
                    
                    ele = span;
                } /*else if( HTMLUtil.nodeNameEquals( ele, "hr" )){
                    para = ele;
                    continue;
                }*/
                
                if( processedConfig.isAllowed.anchor && _isInsideAnchor( _node ) ){
                    
                    var parentAnc = HTMLUtil.getParentNode( _node, "a" ); //NO I18N
                    
                    if( ( anchor && anchor.id != parentAnc.id ) || !anchor ){
                        anchor = HTMLUtil.getParentNode( _node, "a" ).cloneNode(); //NO I18N
                    }
                    
                    /*image paste handling
                      should not use setAttribute otherwise yet-to-load 
                      class will be removed */
 
                    if( ele.hasAttribute( 'data-text-info' ) ){
                        
                        var Attr = JSON.parse( ele.getAttribute( 'data-text-info' ) );
                        
                        if( Attr.hasOwnProperty( 'cls') && Attr.cls.indexOf( "link" ) == -1 ){
                        	Attr.cls += " link";//no i18n
                        } else if( !Attr.hasOwnProperty( 'cls')){//no i18n
                        	Attr.cls = "link";//no i18n
                        }
                        
                        ele.setAttribute( 'data-text-info', JSON.stringify( Attr ) );
                        
                    } else {
                        ele.setAttribute( 'data-text-info', JSON.stringify( { 'cls'    :    'link' } ) );
                        
                    }
                    
                    anchor.appendChild( ele );
                    ele = anchor;
                }
                
                if( !dontPush ){
                    para.appendChild( ele );
                } else{
                    outputEle.appendChild( ele );
                    continue;
                }
            }
        
            if( dontPush ){
                break;
            }
            
            if( !whereToInsert || HTMLUtil.nodeNameEquals( whereToInsert, "body" ) ){
                
                outputEle.appendChild( para );
                
            } else if( HTMLUtil.nodeNameEquals( whereToInsert, "td|th" ) ){//No I18N
                
                var targetTd = outputEle.querySelector( '#' + 
                                        whereToInsert.getAttribute( 'id' ));
                
                if( !targetTd ){
                    
                    
                    var parentTables = HTMLUtil.getParents( whereToInsert, 'table' );//no i18n
                    
                    for( var tableLen = parentTables.length, tableIdx = tableLen - 1; tableIdx >= 0; tableIdx-- ){
                        
                        var currentTable = outputEle.querySelector( '#' + parentTables[ tableIdx ].id );
                        
                        if( !currentTable ){
                            
                            var clonedTable = _getClonedTable( parentTables[ tableIdx ]);
                            
                            var currentTableParent =  HTMLUtil.getParentNode( parentTables[ tableIdx ], [ "td", "th", "body" ]); //NO I18N
                            
                            if( HTMLUtil.nodeNameEquals( currentTableParent, "body" ) ){
                                
                                outputEle.appendChild( clonedTable );
                            
                            } else { //td
                                
                                outputEle.querySelector( '#' + currentTableParent.id ).appendChild( clonedTable );
                                
                            }
                            
                        }
                        
                    }
                                        
                    targetTd = outputEle.querySelector( '#' + whereToInsert.getAttribute( 'id' )) ;
                    
                } 
                
                targetTd.appendChild( para );
                
            }
            
                    
            idx++;    
        
        }
        
 
        _fixTableForSiblingProblem( outputEle );
        
        _fixBrNodes( outputEle );
        
        return outputEle;
    },
    
    _fixBrNodes = function( node ){
        
         /*
             1. br node should contain next sibling 
                otherwise it is not valid
             2. if block element contains only one br then 
                we should create empty para
         */
        
        var brNodes = node.querySelectorAll( 'br' );//no i18n
        for( var idx = 0, len = brNodes.length; idx < len; idx++ ){
            
            var ele = brNodes[ idx ];
            !( ele.nextSibling ) && ele.parentNode.removeChild(ele);
            
        }
        
    },
    
    _getClonedTable = function( table ){
        
        var clonedTable = table.cloneNode( ),
	        rowElems = HTMLUtil.findNodesInTable( table, 'tr' ), //no i18n
	        tableStyles = CSSToJSON.getTableStyles( table );
        
        HTMLUtil.setAttribute( clonedTable,  
                    { 	
	                	'data-table-info' : JSON.stringify( tableStyles ), //NO I18N
	                	"data-row-info" : JSON.stringify( CSSToJSON.getRowStylesInTable( tableStyles ) ), //NO I18N
	                	"data-cell-info" : JSON.stringify( CSSToJSON.getCellStylesInTable( tableStyles ) ) //NO I18N
                    }
                );
        
        for( var rowIdx = 0, row_len = rowElems.length; rowIdx < row_len; rowIdx++ ){
            var clonedRow = rowElems[ rowIdx ].cloneNode( );
            clonedTable.appendChild( clonedRow );
            var rowStyles = CSSToJSON.getRowStyles( rowElems[ rowIdx ] );
            
            HTMLUtil.setAttribute( clonedRow,  
                    { 
                    	"data-row-info" : JSON.stringify( rowStyles ), //NO I18N
                    	"data-cell-info" : JSON.stringify( CSSToJSON.getCellStylesinRow( rowStyles )) //NO I18N
                	}
                );
            
            var cellElems = HTMLUtil.findNodesInTable( rowElems[ rowIdx ], 'td,th' );
            
            for( var cellIdx = 0, cell_len = cellElems.length; cellIdx < cell_len; cellIdx++ ){
                
                var clonedCell = cellElems[ cellIdx ].cloneNode();
                clonedRow.appendChild( clonedCell );
                
                HTMLUtil.setAttribute( clonedCell,  
                        { 'data-cell-info' : JSON.stringify( //NO I18N 
                                                 CSSToJSON.getCellStyles( cellElems[ cellIdx ] )
                                              )
                        }
                    );
                
            }
            
        }
        
        return clonedTable;
        
    };
    
    //these json should be at the bottom since these json contains functions as values which are defined above
    var _processingAllowedFunction = { 	
    		table : _processTableNodes,				anchor : _processAnchors, 					image : _processImgElems,
			iframe : _processIframeElems,			list : _processListElements, 				horizontalRule : _processHr,
			noteSettings : _processNoteContainers, 	conditionMerge : _processConditionalMerge, 	headings : _processHeadings,
			bookmarks : _processBookmarks, 			whiteSpace : _whiteSpacePropertyAndTextNodeHandling,	input	:	_processInputElems,
			comments : _processComments,			textBox	:	_processTextBox
		},

	_processingUnallowedFunction = {
    		list : _processListAsNotAllowed,
    		shape: _processShapeAsNotAllowed
	};
    
    return {
        getDummyNode: _getDummyNode,
        getProcessedHTML    :    createStructure,
        isInsideAnchor        :    _isInsideAnchor,
        processTD            :    _processTD
    };
 
})();