/* $Id$ */
var CSSToJSON = (function() {
	'use strict'; //NO I18N
	
	var allowedTextStyles = ['fontFamily', 'fontSize', 'fontWeight', 'fontStyle', //No i18n
			'fontVariant', 'color', 'text-decoration', //No i18n
			'letter-spacing', 'backgroundColor', 'verticalAlign' //No i18n
		],
 
		allowedParaStyles = ['textIndent', 'textAlign', 'lineHeight', 'direction', //No i18N
			'backgroundColor', 'marginRight', 'marginLeft', //No i18N
			'marginTop', 'marginBottom' , 'borders' //No i18n
		],
		
		inheritableParaStyles = ['textIndent', 'textAlign', 'lineHeight', 'direction', //No i18N
									'backgroundColor', 'marginRight', 'marginLeft', //No i18N
									'marginTop', 'marginBottom' //No i18n
								],
								
		nonInheritableParaStyles = [ 'backgroundColor', 'marginRight', 'marginLeft', //No i18N
									 'marginTop', 'marginBottom', 'borders'],	//no i18n
		
		allowedTextStylesForPara = [ 'color', 'fontSize' ],//No i18n

		allowedTableStyles = ['backgroundColor', 'backgroundImage','text-align','margin-left'], //No i18n

		allowedRowStyles = ['backgroundColor', 'text-align', //No I18N
            'vertical-align', 'paddingLeft', 'height', 'backgroundImage' //No i18n
		],
		
		allowedCellStyles = ['backgroundColor', 'text-align', //No I18N
			'vertical-align', 'paddingLeft', 'height', 'backgroundImage' //No i18n
		],
		
		allowedParaStylesInCell = ['backgroundColor', 'textAlign'], //No I18N
		
		allowedRowStylesInTable = ['backgroundColor', 'text-align'], //No I18N
   		
		allowedCellStylesInTable = ['backgroundColor', 'text-align'], //No I18N
		
		allowedCellStylesinRow = ['backgroundColor', 'text-align', 'height'], //No i18n
		
		_alignmentValues = [ "center", "left", "right", "justify" ],//No i18n
		
		getStyleKeys = function(node, type, allowedStyles ) {
			
			switch (type) {
	
				case NODE_TYPE.PARAGRAPH:
					allowedStyles = inheritableParaStyles;
					break;
				case NODE_TYPE.TABLE:
					allowedStyles = allowedTableStyles;
					break;
				case NODE_TYPE.CELL:
					allowedStyles = allowedCellStyles;
					break;
				case NODE_TYPE.TEXT:
					allowedStyles = allowedTextStyles;
					break;
			}
			
			var keys = [];
			
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/getStyleKeys", [ node, keys ] );//no i18n

			return keys.length ? keys : allowedStyles;
		},

		_getLinkStyles = function(node) {
			var href = node.getAttribute("href"), //No i18N
				target = node.getAttribute("target"); //No i18N
			if (!target) {
				target = "new"; //No i18N
			}
			var linkStyle = {};
			linkStyle.href = href;
			linkStyle.target = target;
			
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/LinkStyles", linkStyle );//No i18N

			return linkStyle;
		},

		_getListStyles = function(node, opStyles) {

			var ls_data, listLvl,
				cls = node.classList;

			if (cls.contains('ms-list')) { //For Word list handling

				ls_data = JSON.parse(node.getAttribute('data-list-info'));
				listLvl = ls_data.l;

			} else if ( node.getAttribute("data-list-info") && 
					( cls.contains( 'zw-check-list-para' ) || cls.contains( 'zw-list'))){//NO I18N

				ls_data = JSON.parse(node.getAttribute("data-list-info"));
				listLvl = ls_data.l;

				ls_data.id = node.getAttribute("data-list-id");
				if( node.hasAttribute( 'data-bullet-id' )){
					ls_data.bullet_id = node.getAttribute( 'data-bullet-id' );
				}
				
			} else {
				if (!HTMLUtil.nodeNameEquals(node, "li")) {

					var parentLi = HTMLUtil.getParentNode(node, "li"); //NO I18N

					if (parentLi) {
						node = parentLi;
					} else {
						return null;
					}
				}

				var listLvlParent = HTMLUtil.getParentNode(node, ["ol", "ul"]), //No I18N
					id = listLvlParent.getAttribute( 'data-list-id' );
                  
				//level should be less than 9
				listLvl = HTMLUtil.getParents( node, [ 'ol','ul' ] ).length - 1;//No I18N
				listLvl = listLvl > 8 ? 8 : listLvl;

				if (node.hasAttribute("data-list-info")) {
					ls_data = JSON.parse(node.getAttribute("data-list-info"));
					ls_data.id = id;
				} else {

					ls_data = {
						'id': id, //NO I18N
						'l': listLvl //NO I18N
					};
				}
			}
			
			ls_data && ( opStyles.ls_data = ls_data );
		},

		
		
		_isValidColor = function(color) {

			if (color && color != "rgba(0, 0, 0, 0)" && color != "transparent") {
				return true;
			} else {
				return false;
			}

		},

		_getNonInheritedStyles = function(node, opStyle) {

			CSSToJSON.getBorderStyles(node, opStyle);

			var parents = HTMLUtil.getParentsUntil(node, "body|td|th"), //No I18N
				marginKeys = ["marginLeft", "marginRight", "marginTop", "marginBottom"], //No I18N
				backgroundKeys = ["backgroundColor"],
				isValidMargin = function(mval) {

					for (var keys = 0, len = marginKeys.length; keys < len; keys++) {
						if (mval[marginKeys[keys]] != "0px") {
							return true;
						}
					}
					return false;

				},

				len = parents.length,
				idx = 0;

			do {

				if (!node) {
					break;
				} else if (node.nodeType != Node.ELEMENT_NODE) {
					node = parents[idx];
					continue;
				}

				var styleKeys = [],
					computedStyles;

				if (!opStyle.hasOwnProperty('marginTop')) {
					styleKeys = styleKeys.concat(marginKeys);
				}

				if (!opStyle.hasOwnProperty('backgroundColor')) {
					styleKeys = styleKeys.concat(backgroundKeys);
				}

				if (styleKeys.length == 0) {
					break;
				}

				var computedStyles = HTMLUtil.getComputedStylesFromBrowser(node, styleKeys);

				if (isValidMargin(computedStyles)) {

					for (var i = 0, len = marginKeys.length; i < len; i++) {
						opStyle[marginKeys[i]] = computedStyles[marginKeys[i]];
					}

				}
				if (_isValidColor(computedStyles.backgroundColor)) {

					opStyle.backgroundColor = computedStyles.backgroundColor;

				}

				node = parents[idx];

			} while (idx++ < len);

		},

		_getBorderStyles = function(node, styles) {

			var borderStyles = ['borderTopWidth', 'borderRightWidth', 'borderBottomWidth', 'borderLeftWidth', //No I18N
					'borderTopColor', 'borderRightColor', 'borderBottomColor', 'borderLeftColor', //No I18N
					'borderTopStyle', 'borderRightStyle', 'borderBottomStyle', 'borderLeftStyle' //No I18N
				],
				borderRadiusKeys = ['borderTopLeftRadius', 'borderTopRightRadius', 'borderBottomLeftRadius', 'borderBottomRightRadius'], //No I18N
				paddingStyles = ["paddingTop", "paddingBottom", "paddingLeft", "paddingRight"]; //No I18N
 
			var isPara = HTMLUtil.nodeNameEquals( node, 'p'),//No I18N

				styleType = isPara || HTMLUtil.nodeNameEquals( node, 'hr') ?//No I18N
				NODE_TYPE.PARAGRAPH : HTMLUtil.nodeNameEquals( node, [ 'td', 'th']) ? NODE_TYPE.CELL : null,//No I18N

				_getBorderRadius = function(node) {

					var info = {},
						_borderRadius = HTMLUtil.getComputedStylesFromBrowser(node, borderRadiusKeys),

						topLeftRadius = Math.round(parseFloat(_borderRadius.borderTopLeftRadius)) || 0,
						topRightRadius = Math.round(parseFloat(_borderRadius.borderTopRightRadius)) || 0,
						bottomLeftRadius = Math.round(parseFloat(_borderRadius.borderBottomLeftRadius)) || 0,
						bottomRightRadius = Math.round(parseFloat(_borderRadius.borderBottomRightRadius)) || 0;
					info = {};
					info["br-topLeft"] = topLeftRadius;//No I18N
					info["br-topRight"] = topRightRadius;//No I18N
					info["br-bottomLeft"] = bottomLeftRadius;//No I18N
					info["br-bottomRight"] = bottomRightRadius;//No I18N

					return info;
				},

				_getBorderInfo = function(node) {

					var info = {},

						_borderStyles = HTMLUtil.getComputedStylesFromBrowser(node, borderStyles),

						topBorderWidth = Math.round(parseFloat(_borderStyles.borderTopWidth)) || 0,
						rightBorderWidth = Math.round(parseFloat(_borderStyles.borderRightWidth)) || 0,
						leftBorderWidth = Math.round(parseFloat(_borderStyles.borderLeftWidth)) || 0,
						bottomBorderWidth = Math.round(parseFloat(_borderStyles.borderBottomWidth)) || 0;
					info.borders = {};
					
					//if one of the border value is avaibale, We need to take of other sides also : P8 - 5802
					 
					info.borders["border-top"] = {//No I18N
							'type': (_borderStyles.borderTopStyle || "none"), //No I18N
							'width': topBorderWidth + "px", //No I18N
							'color': _borderStyles.borderTopColor.replaceAll(" ", "") || "rgb(0,0,0)", //No I18N
							'space': '0px' //No I18N
						};
					info.borders["border-right"] = {//No I18N
							'type': (_borderStyles.borderRightStyle || "none"), //No I18N
							'width': rightBorderWidth + "px", //No I18N
							'color': _borderStyles.borderRightColor.replaceAll(" ", "") || "rgb(0,0,0)", //No I18N
							'space': '0px' //No I18N
						};
					
					info.borders["border-left"] = {//No I18N
						'type': (_borderStyles.borderLeftStyle || "none"), //No I18N
						'width': leftBorderWidth + "px", //No I18N
						'color': _borderStyles.borderLeftColor.replaceAll(" ", "") || "rgb(0,0,0)", //No I18N
						'space': '0px' //No I18N
					};
					
					info.borders["border-bottom"] = {//No I18N
						'type': (_borderStyles.borderBottomStyle || "none"), //No I18N
						'width': bottomBorderWidth + "px", //No I18N
						'color': _borderStyles.borderBottomColor.replaceAll(" ", "") || "rgb(0,0,0)", //No I18N
						'space': '0px' //No I18N
					};
						
					

					if (ZWJSONUtils.isEmptyObject(info.borders)) {
						delete info.borders;
					} else {
						info.borders["border-radius"] = _getBorderRadius(node);//No I18N
					}

					return info;
				},

				_getPaddingInfo = function(node, info, styleType) {

					var paddingKeyMap
					if (styleType == NODE_TYPE.PARAGRAPH) {
						paddingKeyMap = {
							'paddingTop': "border-top", //No I18N
							'paddingBottom': "border-bottom", //No I18N
							'paddingRight': "border-right", //No I18N
							'paddingLeft': "border-left" //No I18N
						}
					} else if (styleType == NODE_TYPE.CELL) {
						paddingKeyMap = {
							'paddingTop': "pt", //No I18N
							'paddingBottom': "pb", //No I18N
							'paddingRight': "pr", //No I18N
							'paddingLeft': "pl" //No I18N
						}
					}
 
					var _paddingStyles,
						paddingStyleKeys = ['paddingTop', 'paddingBottom', 'paddingRight', 'paddingLeft'], //No I18N
						parents = HTMLUtil.getParentsUntil(node, 'body|td|th'), //No I18N
						_setPaddingInfo = function() {

							for (var key in paddingKeyMap) {

								var styleName = key,
									styleValue = _paddingStyles[key];

								styleValue = parseFloat(styleValue.convertor().pixels.toFixed(2)) + 'px'; //No I18N

								if (styleType == NODE_TYPE.PARAGRAPH) {
									info.borders = info.borders || {};
									info.borders[paddingKeyMap[styleName]] = info.borders[paddingKeyMap[styleName]] || {};
									info.borders[paddingKeyMap[styleName]].space = styleValue;
								} else {
									info[paddingKeyMap[styleName]] = styleValue;
								}

							}
						},
						_isValidPadding = function() {

							for (var keys = 0, len = paddingStyleKeys.length; keys < len; keys++) {
								if (_paddingStyles[paddingStyleKeys[keys]]) {
									return true;
								}
							}
							return false;
						},
						idx = 0,
						len = parents.length;

					do {
						if (node.nodeType != Node.ELEMENT_NODE) {
							node = parents[idx];
							continue;
						}
						_paddingStyles = HTMLUtil.getComputedStylesFromBrowser(node, paddingStyleKeys);

						if (_isValidPadding()) {

							_setPaddingInfo();
							break;
						}
						//P8-4428 - [ issue fix start -
						if( node.nodeName.toLowerCase() == 'td' || node.nodeName.toLowerCase() == 'th' ){
							break;
						}
						//- issue fix end ] - P8-4428

						node = parents[idx];

					} while (idx++ < len);

				};

			var borderInfo, targetNodeForBorder = node;

			if (styleType == NODE_TYPE.PARAGRAPH) {

				var borderTemplate = {};
				borderTemplate.borders = {};
				var borderWidth = '0px'; //no i18N
				var borderStyle = 'none'; //no i18N
				var borderColor = 'rgb(0,0,0)'; //no i18N
				borderTemplate.borders["border-top"] = {//No I18N
					'type': borderStyle, //No I18N
					'width': borderWidth, //No I18N
					'color': borderColor //No I18N
				};
				borderTemplate.borders["border-right"] = {//No I18N
					'type': borderStyle, //No I18N
					'width': borderWidth, //No I18N
					'color': borderColor //No I18N
				};
				borderTemplate.borders["border-bottom"] = {//No I18N
					'type': borderStyle, //No I18N
					'width': borderWidth, //No I18N
					'color': borderColor //No I18N
				};
				borderTemplate.borders["border-left"] = {//No I18N
					'type': borderStyle, //No I18N
					'width': borderWidth, //No I18N
					'color': borderColor //No I18N
				};
				var parents = HTMLUtil.getParentsUntil(node, 'body|td|th'); //No I18N

				borderInfo = _getBorderInfo(node);

				if (ZWJSONUtils.isEmptyObject(borderInfo)) {

					for (var len = parents.length, idx = len - 1; idx >= 0; idx--) {

						if (parents[idx].nodeType != Node.ELEMENT_NODE) {
							continue;
						}

						borderInfo = _getBorderInfo(parents[idx]);
						if (!ZWJSONUtils.isEmptyObject(borderInfo)) {
							targetNodeForBorder = parents[idx];
							break;
						}

					}

				}

				if (ZWJSONUtils.isEmptyObject(borderInfo)) {
					borderInfo = borderTemplate;
				}

			} else {
				borderInfo = _getBorderInfo(node);
			}
			if (styleType) {
				_getPaddingInfo(targetNodeForBorder, borderInfo, styleType);
			}

			extend(styles, borderInfo);

		},
		
		_getRowStyles = function(row) {
			
			var computedStyles = HTMLUtil.getComputedStylesFromBrowser(row, allowedRowStyles),
				styles = {};
			
			CSSToJSON.getBorderStyles(row, styles);
			
			for (var key in computedStyles) {
				var styleName = key,
					styleValue = computedStyles[styleName];

				if (styleName == "text-align" && styleValue.indexOf( _alignmentValues ) != -1) { //No I18n
					styles[ styleName ] = styleValue;
				} else if (styleName == "vertical-align" && //NO I18N 
					styleValue != "baseline") { // No I18N
					styles[ styleName ] = styleValue;
				} else if (styleName == "backgroundColor" && _isValidColor(styleValue)){ //NO I18N
					styles[ styleName ] = styleValue;
				} else if (styleName == "backgroundImage" && getValidBgImgSrc( styleValue ) != "none" ) { //NO I18N
					styles[ styleName ] = getValidBgImgSrc( styleValue );
				} else if (styleName == "height" && parseFloat(styleValue) > 0) { //NO I18N
					styles[ styleName ] = parseFloat( styleValue );
				}
			}

			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/RowStyles", [ row, styles ] );//no i18n
			return styles;
		},

		_getCellStyles = function(cell) {

			var cellStyleKeys = getStyleKeys(cell, NODE_TYPE.CELL),
				computedStyles = HTMLUtil.getComputedStylesFromBrowser(cell, cellStyleKeys),
				styles = {},
				merge = cell.getAttribute( 'merge' ), //No I18N
				colspan = cell.getAttribute('colspan'),
				rowspan = cell.getAttribute('rowspan');
						
			if (colspan) {
				styles.colspan  = parseInt(colspan);
				if (!rowspan) {
					styles.rowspan = 1;
				}
				styles.merge = 'fc'; //No i18n
			}

			if (rowspan) {
				styles.rowspan = parseInt(rowspan);
				if (!colspan) {
					styles.colspan = 1;
				}
				styles.merge = 'fc'; //No i18n
			}

			
			merge && ( styles.merge = merge );
			CSSToJSON.getBorderStyles(cell, styles);

			for (var key in computedStyles) {
				var styleName = key,
					styleValue = computedStyles[styleName];

				if (styleName == "text-align" && _alignmentValues.indexOf( styleValue ) != -1) {//NO I18N
					styles[styleName] = styleValue;
				} else if (styleName == "vertical-align" && //NO I18N 
					styleValue != "baseline") { // No I18N
					styles[styleName] = styleValue;
				} else if (styleName == "backgroundColor" && _isValidColor( styleValue )) { //NO I18N
					styles[styleName] = styleValue;
				} else if (styleName == "backgroundImage" && getValidBgImgSrc( styleValue ) != "none"){ //NO I18N
					styles[ styleName ] = getValidBgImgSrc( styleValue );
				} else if (styleName == "height" && parseFloat(styleValue) > 0) { //NO I18N
					styles[styleName] = styleValue;
				}

			}
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/CellStyles", [ cell, styles ] );//no i18n
			return styles;
		},
		
		getParaStyleFromCell = function( node ){
			
			return _getParagraphStylesOfBlockEle( node, allowedParaStylesInCell );
			
		},
		
		getCellStylesinRow = function( rowStyles ){
			var cellStyles = {};
			
			for( var key in rowStyles ){
				if( allowedCellStylesinRow.indexOf( key ) != -1 ){
					cellStyles[ key ] = rowStyles[ key ];
				}
			}
			
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/CellStyles", [ null, cellStyles ] );//no i18n
			
			return cellStyles;
		},
		
		getCellStylesInTable = function( tableStyles ){
			var cellStyles = {};
			
			for( var key in tableStyles ){
				if( allowedCellStylesInTable.indexOf( key ) != -1 ){
					cellStyles[ key ] = tableStyles[ key ];
				}
			}
			
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/CellStyles", [ null, cellStyles ] );//no i18n
			
			return cellStyles;
		},
		
		getRowStylesInTable = function( tableStyles ){
			var rowStyles = {};
			
			for( var key in tableStyles ){
				if( allowedRowStylesInTable.indexOf( key ) != -1 ){
					rowStyles[ key ] = tableStyles[ key ];
				}
			}
			
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/RowStyles", [ null, rowStyles ] );//no i18n
			
			return rowStyles;
		},

		_getTableStyles = function(table) {
			
			var styles = {};

			if (!table) {
				return styles;
			}

			CSSToJSON.getBorderStyles(table, styles);

			var len = allowedTableStyles.length,
				computedStyles = {},
				tableStyleKeys = getStyleKeys(table, NODE_TYPE.TABLE);
			
			computedStyles = HTMLUtil.getComputedStylesFromBrowser(table, tableStyleKeys);
			computedStyles.backgroundImage = getValidBgImgSrc(computedStyles.backgroundImage);

			for (var key in computedStyles) {
				var styleName = key,
					styleValue = computedStyles[styleName];

				if (styleName == "text-align" && styleValue.indexOf( _alignmentValues ) != -1) {//NO I18N
					styles[styleName] = styleValue;
				} else if (styleName == "backgroundColor" && _isValidColor( styleValue )) { //NO I18N
					styles[styleName] = styleValue;
				} else if (styleName == "backgroundImage" && styleValue != "none") { //NO I18N
					styles[styleName] = styleValue;
				} else if( styleName == "margin-left" ){ //NO I18N
					styles[styleName] = styleValue;
				}
 
			}
			
			table.hasAttribute('align') && ( styles.align = table.getAttribute('align') );//no i18n
			table.hasAttribute("cellpadding") && ( styles.cellpadding = table.getAttribute('cellpadding') );//no i18n
			
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/TableStyles", [ table, styles ] );//no i18n
			
			return styles;
		},
		
		_getTextStylesOfBlockEle = function( node ){
			return _getTextStylesOfTextNode( node, allowedTextStylesForPara );
		},

		_getParagraphStylesOfBlockEle = function(node, allowedStyles) {

			var paragraphStyles = getStyleKeys(node, NODE_TYPE.PARAGRAPH),
				paraStyles = HTMLUtil.getComputedStylesFromBrowser(node, paragraphStyles); //P8-2782

			var headingParent = HTMLUtil.getParentNode(node, ["h1", "h2", "h3", "h4", "h5", "h6"]), //No I18N
				headingVal = 0;

			if (HTMLUtil.nodeNameEquals(node, "h1|h2|h3|h4|h5|h6")) {
				headingParent = node;
			}

			if (headingParent) {
				headingVal = headingParent.nodeName.substr(1);
				paraStyles.heading = headingVal;
			}

			_getNonInheritedStyles(node, paraStyles);

			for (var key in paraStyles) {

				var styleName = key,
					styleValue = paraStyles[styleName];

				if (!styleValue || styleValue == "normal") {
					delete paraStyles[ key ];
					continue;
				}

				if (styleName == "textAlign") { //No I18N
					//refer :: developer.mozilla.org/en-US/docs/Web/CSS/text-align
					if (styleValue.indexOf("center") != -1) {
						styleValue = "center"; //No I18N
					}

				} else if (styleName == "marginRight" || styleName == "marginLeft") { //No I18N

					styleValue = styleValue.convertor().inches;
 
					styleValue = parseFloat(styleValue.toFixed(3)) + "in"; //No I18N

				} else if (styleName == "marginTop" || styleName == "marginBottom") { // No I18N

					styleValue = styleValue.convertor().points;

					if (styleValue < 0) {
						styleValue = 0;
					}

					styleValue = parseFloat(styleValue.toFixed(2)) + "pt"; //No I18N

				}
				else if (styleName == "textIndent") { //No I18N

					styleValue = parseFloat(styleValue.convertor().inches.toFixed(2)) + "in"; //No I18N

				} else if (styleName == "lineHeight") { //No I18N

					//refer :: developer.mozilla.org/en/docs/Web/CSS/line-height

					var fontSize = HTMLUtil.getComputedStylesFromBrowser(node, ["font-size"])["font-size"], //NO I18N
						lineHeightInPx = styleValue.convertor().pixels,
						fontSizeInPx = fontSize.convertor().pixels;

					styleValue = lineHeightInPx / fontSizeInPx;

					styleValue = parseFloat(styleValue.toFixed(1));

				} else if (styleName == "backgroundColor" && styleValue == "transparent") { //NO I18N

					styleValue = null;

				}

				if (styleValue != null) {
					paraStyles[styleName] = styleValue;
				}
			}

			_getListStyles(node, paraStyles);
			
			if( allowedStyles && allowedStyles.length ){
				for( var key in paraStyles ){
					if( allowedStyles.indexOf( key ) == -1 ){
						delete paraStyles[ key ];
					}
				}
			}
 			
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/ParagraphStyles", [ node, paraStyles ] );//no i18n
			return paraStyles;
 
		},
 
		_getTextStylesOfTextNode = function( node, allowedStyles ) {

			//Should be used only for text Node
			/*if (node.nodeType != 3) {
				return null;
			}*/

			//In case of text Node look styles for parent node for text styles
			if (node.nodeType == 3) {
				node = node.parentNode;
				if (!node) {
					return {};
				}
			}

			var nodeStyles = getStyleKeys(node, NODE_TYPE.TEXT),
				spanStyles = HTMLUtil.getComputedStylesFromBrowser(node, nodeStyles),
				tdInfo = _getTextDecorationInfo(node);
			
			//Text decoration style can't be inherited like margin, border and padding
			if (tdInfo) {
				spanStyles["text-decoration"] = tdInfo; //No I18N
			}

			spanStyles.backgroundColor = _isValidColor( spanStyles.backgroundColor ) ? spanStyles.backgroundColor : _getBackgroundColor( node );
			if (nodeStyles.indexOf('verticalAlign') != -1) {
				spanStyles.verticalAlign = _getVerticalAlign(node);
			}

			for ( var key in spanStyles ){

				var styleName = key,
					styleValue = spanStyles[styleName];

				if (!styleValue) {
					continue;
				}

				if (styleName == "fontWeight") { //NO I18N

					if (isNaN(styleValue)) {
                                            switch(styleValue){
                                                case "lighter": styleValue = 100; break; //No I18N
                                                case "bolder": styleValue = 700; break; //No I18N
                                                case "normal": //No I18N
                                                case "bold": break; //No I18N
                                                default: styleValue = "normal"; //No I18N
                                            }
					} else {
                                            styleValue = parseInt(styleValue);
                                            if (styleValue<100) {
                                                styleValue = 100;
                                            } else if (styleValue>900) {
                                                styleValue = 900;
                                            } else {
                                                styleValue = parseInt(styleValue/100)*100;
                                            }
                                        }

				} else if (styleName == "fontSize") { //NO I18N

					//return in points
					styleValue = (styleValue.convertor().points).toFixed(2);

				} else if (styleName == "color") { //NO I18N 

					//Refer :: stackoverflow.com/questions/4774022/whats-default-html-css-link-color
					if ((HTMLUtil.nodeNameEquals(node, "a") || HTMLPreProcessor.isInsideAnchor(node)) &&
						(ZWColorUtils.rgbToHex(styleValue).toUpperCase() == "#0000EE" && node.style.color == "")) { //No I18N

						styleValue = node.style.color;

					}

				}
				else if (styleName === "verticalAlign" && //No I18N
					(styleValue === "super" || styleValue === "sub")) { //No I18N

					var size = spanStyles.fontSize.convertor().points;
					size = parseInt(size * (1 / 0.7));

					spanStyles.fontSize = size;

				} else if (styleName == "letter-spacing") { //NO I18N

					styleValue = styleValue == "normal" ? "0pt" : styleValue; //NO I18N
					// Math.round(x*100)/100 ---> is done for rounding up the value for 2 decimal places
					styleValue = Math.round(styleValue.convertor().points * 100) / 100 + "pt"; //NO I18N

				} else if (styleName == "backgroundColor" && styleValue == "transparent") { //NO I18N

					styleValue = null;

				} else if ( styleName == "text-decoration" ){ //NO I18N
					
					if ((HTMLUtil.nodeNameEquals(node, "a") || HTMLPreProcessor.isInsideAnchor(node)) &&
						(styleValue == "underline" && node.style.textDecoration != "underline")) { //No I18N

						styleValue = node.style.textDecoration;

					}
				}

				if (styleValue != null) {
					spanStyles[styleName] = styleValue;
				}
			}
			
			if( allowedStyles && allowedStyles.length ){
				for( var key in spanStyles ){
					if( allowedStyles.indexOf( key ) == -1 ){
						delete spanStyles[ key ];
					}
				}
			}
			
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/TextStyles", [ node, spanStyles ] );//no i18n
			return spanStyles;
		},

		_getVerticalAlign = function(node) {

			var parents = HTMLUtil.getParentsUntil(node, "body|td|th"), //No I18N
				vA = HTMLUtil.getComputedStylesFromBrowser(node, ["verticalAlign"]).verticalAlign; //No I18N

			if (vA == "super" || vA == "sub") { //No i18N
				return vA;
			}

			for (var idx = 0, len = parents.length; idx < len; idx++) {
				if (parents[idx].nodeType != Node.ELEMENT_NODE) {
					continue;
				}

				vA = HTMLUtil.getComputedStylesFromBrowser(parents[idx], ["verticalAlign"]).verticalAlign; //No I18N
				if (vA == "super" || vA == "sub") { //No i18N
					break;
				}

			}

			return vA;

		},
 
		getValidBgImgSrc = function(imgSrc) {

			var url = ""; //No i18N
			try {
				if (imgSrc && imgSrc.indexOf('url') != -1) {
					imgSrc = imgSrc.replace(/url\(("|)|((|")\))/g, ""); //No i18N
				}
			} catch (ex) {
				imgSrc = "none"; //No I18N
			}
			return imgSrc;
		},

		_getBackgroundColor = function(node) {

			var parents = HTMLUtil.getParentsUntil(node, "body|td|th"), //No I18N
				bgColor = "rgba(0, 0, 0, 0)"; //No I18N

			for (var idx = 0, len = parents.length; idx < len; idx++) {
				
				if( parents[ idx ].nodeType != Node.ELEMENT_NODE ){
					continue;
				}
				
				var styles = HTMLUtil.getComputedStylesFromBrowser(parents[ idx ],["backgroundColor"]);
				
				if (_isValidColor(styles.backgroundColor)) {
					bgColor = styles.backgroundColor;
					break;
				}

			}

			return bgColor;
		},

		_getTextDecorationInfo = function(node) {

			var parents = HTMLUtil.getParentsUntil(node, "body"), //No I18N
				isUnderLine = false,
				isLineThrough = false;
			
			if( node.nodeType == Node.ELEMENT_NODE ){
				parents.unshift( node );
			}

			for (var idx = 0, len = parents.length; idx < len; idx++) {

				if (parents[idx].nodeType == Node.DOCUMENT_FRAGMENT_NODE) {
					continue;
				}

				//textDecorationLine not supported in Safari, Moving to textDecoration
				var styles = ['textDecoration'], //No i18N
					textDecoration = HTMLUtil.getComputedStylesFromBrowser(parents[idx], styles).textDecoration;

				if (textDecoration.indexOf("none") == -1) { //No i18N

					if (!isUnderLine &&
						textDecoration.indexOf("underline") != -1) { //No i18N
						isUnderLine = true;
					}

					if (!isLineThrough &&
						textDecoration.indexOf("line-through") != -1) { //No i18N
						isLineThrough = true;
					}

				}

			}

			var tdInfo = isUnderLine ? "underline " : ""; //No I18N
			tdInfo += isLineThrough ? "line-through" : ""; //No I18N

			return tdInfo.trim();

		},

		_getImageStyles = function(node) {

			var dataFmt = node.getAttribute("data-format"), //No i18N
				computedStyle = HTMLUtil.getComputedStylesFromBrowser(node, 
						['width', 'height', 'transform', 'opacity', 'top',//NO I18N
						'left', 'marginTop', 'marginBottom', 'marginLeft', 'marginRight']); //NO I18N
			
			CSSToJSON.getBorderStyles(node, computedStyle);
			
			for( var key in computedStyle ){
				var styleName = key,
					styleValue = computedStyle[ styleName ];
				
				switch( styleName ){
					case "transform" :	if (styleValue != 'none' && styleValue.length ) { // NO I18N
											var values = styleValue.match( /matrix\((.+?)\)/ );    
											if( values[ 1 ] ){    
											    var degrees = values[ 1 ].split( "," ),
											        degAttr1 = degrees[ 0 ],
											        degAttr2 = degrees[ 1 ];
											   
										        var angle = Math.round( Math.atan2( degAttr2 , degAttr1 ) * ( 180/Math.PI ) );
										        if( angle  <  0  )  {
										          angle  = angle+360;
										        }
										        computedStyle.transform = angle;
											}
										} else {
											delete computedStyle[ styleName ];
										}
										break;
					case "height"	:	//NO I18N
					case "width"	:	styleValue = parseInt(styleValue);	//NO I18N
										computedStyle[styleName] = !styleValue ? 0 : styleValue;
										break;
					case "top"		:	//NO I18N
					case "left"		:	styleValue = parseInt(styleValue, 10),//NO I18N
										computedStyle[styleName] = !styleValue ? 0 : styleValue;
										break;
				}
			}
			
			var nodeStyle = node.style,
				align = node.getAttribute('align') || nodeStyle.float,
				src = node.getAttribute( 'src' );

			align && ( computedStyle.align = align );
			src && ( computedStyle.src = src );
			
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/ImageStyles", [ node, computedStyle ] );//no i18n
			
			return computedStyle;
		},

		_getIframeStyles = function(node) {

			var styles = {},
				computedStyle = HTMLUtil.getComputedStylesFromBrowser(node, ['width', 'height']), //NO I18N
				width = parseInt(computedStyle.width),
				height = parseInt(computedStyle.height),
				src = node.getAttribute('src'); //No i18N

			styles.width = width; //No i18N
			styles.height = height; //No i18N
			styles.src = src;
			
			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/IframeStyles", [ node, styles ] );//no i18n
			
			return styles;
		},

		_getHRStyles = function(node) {

			var styles = {};
			
			if (node.hasAttribute('width')) {
				var computedStyle = HTMLUtil.getComputedStylesFromBrowser(node, ['width']), //NO I18N
					width = parseInt(computedStyle.width);

				styles.width = width;
			}

			CSSToJSON.getBorderStyles(node, styles);

			typeof PubSub != "undefined" &&  PubSub.publish( "CSSTOJSON/HRStyles", [ node, styles ] );//no i18n
			return styles;
		};

	return {
		getListStyles: _getListStyles,
		getRowStyles: _getRowStyles,
		getBorderStyles: _getBorderStyles,
		getCellStyles: _getCellStyles,
		getTableStyles: _getTableStyles,
		getParagraphStylesOfBlockEle: _getParagraphStylesOfBlockEle,
		getTextStylesOfBlockEle	:	_getTextStylesOfBlockEle,
		getTextStylesOfTextNode: _getTextStylesOfTextNode,
		getBackgroundColor: _getBackgroundColor,
		getTextDecorationInfo: _getTextDecorationInfo,
		getImageStyles: _getImageStyles,
		getIframeStyles: _getIframeStyles,
		getLinkStyles: _getLinkStyles,
		getHRStyles: _getHRStyles,
		getRowStylesInTable	:	getRowStylesInTable,
		getCellStylesInTable:	getCellStylesInTable,
		getCellStylesinRow	:	getCellStylesinRow,
		getParaStyleFromCell:	getParaStyleFromCell
	}
 
})();