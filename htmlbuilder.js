/* $Id$ */
HTMLBuilder = ( function(){

	'use strict';//No I18N

	var nbspChr = "\u00a0",//No I18N

	_paraStyleKeyDataAttrMap = {};

	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.PS_ML ] = 'data-margin-left';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.PS_TI ] = 'data-text-indent';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.PS_PL ] = 'data-padding-left';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.PS_SA ] = 'data-margin-bottom';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.PS_SB ] = 'data-margin-top';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.PS_HD ] = 'data-hd-info';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.PS_TPS ] = 'data-tab-info';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.EN_BL ] = 'data-keep-line-info';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.EN_PB ] = 'data-page-break-info';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.EN_WO ] = 'data-window-control-info';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.EN_KN ] = 'data-keep-next-info';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.PS_TS ] = 'data_styles';
	_paraStyleKeyDataAttrMap[ ATTR_PARAGRAPH.PS_CODE ] = 'data-codeformat';


	var getParagraph = function( dtPara ){

		var segments = dtPara.segments,
			dtParaEnd = dtPara.getEndIndex(),
			computedFmt = editor.doc.getComputedFormatAt( dtParaEnd, NODE_TYPE.PARAGRAPH ),
			headingVal = computedFmt[ ATTR_PARAGRAPH.PS_HD ] || 0,
			tagName = 'p', //No I18N
			attr = {},
			_isList = ListUtils.isList( computedFmt ),
			paraNode = document.createElement( tagName ),
			styles;

		/*
		 * if custom paragraph style along with list styles, We should carry
		 * margins, padding left and text indent via data attributes
		 */

		for( var _key in _paraStyleKeyDataAttrMap ){
			if( computedFmt.hasOwnProperty( _key )){
				var _val = computedFmt[ _key ];
				if( ( typeof _val).toLowerCase() === "object" ){
					 _val = JSON.stringify( _val );
				}
				attr[ _paraStyleKeyDataAttrMap[ _key ]] = _val;
			}
		}

		//for list element strip margin-Left & text indent
		if( _isList ){

			if( computedFmt.hasOwnProperty( ATTR_PARAGRAPH.PS_ML )){ //p8 - 5561
				attr[ 'data-margin-left' ] = computedFmt[ ATTR_PARAGRAPH.PS_ML ];
			}
			if( computedFmt.hasOwnProperty( ATTR_PARAGRAPH.PS_TI )){
				attr[ 'data-text-indent' ] = computedFmt[ ATTR_PARAGRAPH.PS_TI ];
			}
			if( computedFmt.hasOwnProperty( ATTR_PARAGRAPH.PS_PL )){
				attr[ 'data-padding-left' ] = computedFmt[ ATTR_PARAGRAPH.PS_PL ];
			}

			if( computedFmt.hasOwnProperty( ATTR_PARAGRAPH.PS_SA )){
				attr[ 'data-margin-bottom' ] = computedFmt[ ATTR_PARAGRAPH.PS_SA ];
			}

			if( computedFmt.hasOwnProperty( ATTR_PARAGRAPH.PS_SB )){
				attr[ 'data-margin-top' ] = computedFmt[ ATTR_PARAGRAPH.PS_SB ];
			}

			var ls_data = computedFmt[ ATTR_PARAGRAPH.LS_DATA ],
				listId =  ls_data[ ATTR_COMMON.ID ],
				isBasicList = ListUtils.isBasicList( dtPara ),
				isHeadingList = ListUtils.isHeadingList( dtPara );

			!isBasicList && _appendListCharNode( paraNode, dtPara, computedFmt );

			if( !isHeadingList ){
				delete computedFmt[ ATTR_PARAGRAPH.PS_ML ];
				delete computedFmt[ ATTR_PARAGRAPH.PS_TI ];


				if( CheckListOperations.isCheckList( computedFmt )){
					var listAttr =  editor.doc.getListObj( listId ),
					splBulletInfo = ListUtils.getSplBulletInfo( listAttr );

					attr[ 'data-list-format' ]			=	JSON.stringify( listAttr ),//No I18N
					attr[ 'data-spl-bullet-format' ]	=	JSON.stringify( splBulletInfo );//No I18N

					J( paraNode ).addClass( 'zw-check-list-para' );
				}

				attr[ 'data-list-id' ] = listId;//No I18N
				attr[ 'data-list-info' ] = JSON.stringify( ls_data ); //NO I18N

				if( ls_data.hasOwnProperty( ATTR_COMMON.BULLET_ID )){
					attr[ 'data-bullet-id' ] = ls_data.bullet_id;//NO I18N
				}

				/*
				if( headingVal <= 9 && ls_data && ls_data.id == editor.doc.getHeadingListId() ) {
					attr[ 'data-hlist-type' ] = editor.doc.getBodyFormat().listFormat[0].level0.htype;
				}
				*/

				J( paraNode ).addClass( 'zw-list' );
			}
		}

		//P8-3522
		var styles = { "margin" : "0px" };//NO I18N
		J( paraNode ).css( styles );

		//if( !computedFmt[ATTR_PARAGRAPH.PS_CODE ]){
			DomUtil.setParaStyle( paraNode, computedFmt, null, true );
		//}

		if( computedFmt[ ATTR_COMMON.BORDERS ] ){
			var borders = computedFmt[ ATTR_COMMON.BORDERS ],
			border_sides = [],
			borderInfo = {};

			if( borders[ ATTR_BORDERS.BORDER_BETWEEN ] ){
				borderInfo[ 'border-between' ] = borders[ ATTR_BORDERS.BORDER_BETWEEN ];
			}

			for( var key in borders ){
					border_sides.push(key);
			}

			borderInfo[ 'border-sides' ] = border_sides; //NO I18N
			attr[ 'data-writer-border-info' ] = JSON.stringify( borderInfo ); //NO I18N
		}

		J( paraNode ).attr( attr );

		//add EOP span
		var newSpan = document.createElement( 'span' );
		J( newSpan ).addClass( "EOP" ); //No I18N
        newSpan.innerHTML = "&nbsp;"; //No I18N
        paraNode.appendChild(newSpan);

        J( paraNode ).addClass( "zw-paragraph" );

		return paraNode;
	},

	getTable = function( dtTable ){

		var src = null,
		styles = {},imgURL,
		formatAttr = dtTable.getFormat();

		var table = document.createElement( 'table' ),
		dtTableStart = dtTable.getIndex(),
		fmt = editor.doc.getThemeStyleFormatAt( dtTableStart );
		imgURL = fmt[ ATTR_TABLE.TBLS_BGIMG ];

		imgURL = imgURL && imgURL.substr( -4 ) != "none" ? imgURL : '';//NO I18n

		var bgImageName = fmt[ ATTR_TABLE.TBLS_BGIMG_NAME ];

		imgURL = HTMLUtil.getImageData( imgURL );
		styles = {
					'backgroundColor'	:	fmt[ ATTR_TABLE.TBLS_TBL_BGC],
					'marginLeft'		:	fmt[ ATTR_TABLE.TBLS_ML ]
				 };
		var wid = fmt[ ATTR_TABLE.TBLS_WID ],
		dataAttr = {
						'data-theme'		:	JSON.stringify( fmt[ ATTR_TABLE.TBLS_THEME ]),
						'data-cols'			:	JSON.stringify( dtTable.cols ), //NO I18N
						'data-width'		:	wid//No I18N
					};

		if( imgURL && bgImageName ){
			dataAttr[ 'data-bgimg-name' ] = bgImageName;//no i18n
		}

		J( table ).attr( dataAttr )
		.addClass( 'zw-table' );

		if( imgURL ){
			extend( styles , {'backgroundImage': 'url(' +imgURL+ ')' } );//NO I18N
		}
		J( table ).css( styles );
		return table;
	},

	getRow = function( dtRow ){
		var row = document.createElement( 'tr' ),
		dtRowStart = dtRow.getIndex(), dataAttr,
		fmt = editor.doc.getThemeStyleFormatAt( dtRowStart ),
		rowFmtObj = {};

		if( fmt.htRule ){
			rowFmtObj.htRule = fmt.htRule;
		}

		dataAttr = {
				"data-row-info"		:	JSON.stringify( rowFmtObj ) //NO I18N
		}

		J( row ).attr( dataAttr );

		return row;
	},

	getCell = function( dtCell ){
		var dtCellStart = dtCell.getIndex(),
		styles = {},
        formatAttr = dtCell.getFormat(),attr,
        colIndex = dtCell.getMatrixPosition()[1],
        fmt = DocUtil.getRenderingFormat( editor.doc.getThemeStyleFormatAt( dtCellStart ) ),
        bgImageName = fmt[ ATTR_TABLE.TBLS_BGIMG_NAME ],
        cell = DomUtil.createTD( dtCell, TableUtil.generateFormatForCell( dtCell ) );

        extend( styles, { 'width' : dtCell.getCellWidth() } )//NO I18N

        var imgURL = HTMLUtil.getImageData( fmt[ ATTR_CELL.TDS_BGIMG ] );

       	imgURL && extend( styles, {
        					'backgroundImage': 'url('+ imgURL +')'//No I18N
        				 });

        if( typeof fmt[ ATTR_CELL.TDS_HT ] != "undefined" ){
        	styles.height = fmt[ ATTR_CELL.TDS_HT ];
        } else {
            styles.height = 'auto';
        }

        if(typeof fmt[ATTR_CELL.TDS_VA] == 'undefined') {
            styles.verticalAlign = 'top'; //No I18N
        }

        J( cell ).css( styles );
        //TODO :: need to change during Table PR
        var cellEle = _getDocElementFromDTNode( dtCell, NODE_TYPE.CELL ),
        rowEle = cellEle.parentNode(),
        tableEle = rowEle.parentNode();

        var colIdx = TableUtil.findColIdx( cellEle ),
        rowIdx = TableUtil.findRowIdx( rowEle );
        var bdrStyle =  BorderUtils.getTdBdrDomStyle( cellEle);

        var borderObj = {};
        for( key in bdrStyle ){
        	var val = bdrStyle[ key ];
        	borderObj[ key ] = val;
        }
        borderObj && J( cell ).css( borderObj ).addClass( 'zw-td' );

        if( fmt[ ATTR_CELL.TDS_MRG ] && fmt[ ATTR_CELL.TDS_MRG ] != "fc" ){
        	J( cell ).hide();
		}

        attr = {
        			'col-index' : colIndex, //NO I18N
        			'data-cell-height' : styles.height //NO I18N
        		}
        if(bgImageName){
        	attr['data-bgimg-name'] = bgImageName;//no i18n
        }
        J( cell ).attr( attr );
		return cell;
	},

	_getDocElementFromDTNode = function( dtNode, type ){
		var docEle = editor.doc.createElement( type, dtNode );

		docEle.setStart( dtNode.getIndex() ),
		docEle.setEnd( dtNode.getEndIndex() );

		return docEle;
	},

	getHR	=	function( dtNode ){
		var hr = DomUtil.createHR(),
		hrView = ViewManager.getViews(dtNode),
		fmt = editor.doc.getRawFormatAt( dtNode.getEndIndex() );
		DomUtil.setHRStyle( hr, fmt, hrView, true );
		hr.setAttribute( 'data-hr-info', JSON.stringify( fmt ) );
		return hr;
	},

	createListElement = function( isOrdered, listId, fromLevel, toLevel ){

		var listAttr, splBulletInfo;

		if( fromLevel == null ){ //create First level
			fromLevel = -1;
			toLevel = toLevel != null ? toLevel : 0;
			listAttr =  editor.doc.getListObj(  listId );
			splBulletInfo = ListUtils.getSplBulletInfo( listAttr );
		}

		var idx = fromLevel,
		listElem, parentListElem;

		do {
			idx++;
			var newListElem = document.createElement( isOrdered ? 'ol' : 'ul' ),
			listType = editor.doc.getListType( listId, idx );

			if( listType == LIST_TYPE.SYMBOL || listType == LIST_TYPE.IMAGE ){
				newListElem.style.listStyle = ListUtils.getHTMLListType( LIST_TYPE.DISC );
			} else {
				newListElem.style.listStyle = ListUtils.getHTMLListType( listType );
			}

			if( listElem ){
				listElem.appendChild( newListElem );
			} else{
				parentListElem = newListElem;
			}

			listElem = newListElem;
		} while( idx < toLevel )

		var attr = {};
		attr[ 'data-list-id' ] = listId;//No I18N
		attr[ 'data-list-format' ] = listAttr && JSON.stringify( listAttr );//No I18N
		attr[ 'data-spl-bullet-format' ] = splBulletInfo && JSON.stringify( splBulletInfo );//No I18N

		J( parentListElem ).attr( attr );

		return parentListElem;
	},

	_getContent = function( posStart, posEnd ){
		var content = editor.doc.getContent( posStart, posEnd ),
		len = content.length, chars = '';
		
		//single span with space char alone
		if( len == 1 && /\s/g.test(content)){
			return nbspChr;
		}

		for(var idx = 0; idx < len; idx++ ){
			var ch = content.substr( idx, 1 );
			ch = Utils.encodeHTMLChars(ch);
			chars += ch;
		}	
		
		if( /\s{2,}/g.test(content) ){
			var array, lastIndex = 0, newStr = "",
				regex = RegExp(/\s{2,}/g);
			
			while ((array = regex.exec(content)) !== null) {
			    newStr += content.substr( lastIndex, array.index-lastIndex ); 
			    var noOfSpaces = array[ 0 ].length; 
			    while( noOfSpaces > 0 ){
			      newStr += nbspChr;
			      noOfSpaces--;
			    }
			    lastIndex = array.index + array[ 0 ].length;
			}
			if( lastIndex < content.length ){
				newStr += content.substr( lastIndex, content.length - lastIndex );
			}
			content = newStr;
		}

		return content;
	},

	getContentFormatArr = function( start, end ){
		var fmt = ZWJSONUtils.clone( editor.doc.getFormatsBetween( start, end ) ),
		listFormat = {}, splBulletInfo = {};

		for( var idx = 0, len = end - start; idx < len; idx++ ){
			var ls_dataId = fmt[ idx ] && fmt[ idx ].ls_data && fmt[ idx ].ls_data.id;

			if( ls_dataId != null && typeof ls_dataId != "undefined" ){ //NO I18N
				listFormat[ ls_dataId ] = editor.doc.getListObj( ls_dataId );
				var bulletInfo = ListUtils.getSplBulletInfo( listFormat[ ls_dataId ] );
				!U.isEmpty( bulletInfo ) && extend( splBulletInfo, bulletInfo );

			}
		}

		return {
			'fmt' :	fmt,//NO I18N
			'listFormat' :	listFormat, //NO I18N
			'bulletFormat' : splBulletInfo //NO I18N
		};
	},

	_appendListCharNode = function( domPara, dtNode, format ){

		var charNode;

		if( CheckListOperations.isCheckList( format ) ||
				ListUtils.isSymbolList( dtNode ) ||
				ListUtils.isHeadingList( dtNode ) ||
				ListUtils.isModifiedListChar( dtNode )){

			var char = CheckListOperations.isCheckList( format ) ?
						CheckListOperations.getCheckListChar(format) :
						dtNode.paraProperties.listData.listNumberString,
				renderListData = ViewManager.getListData( dtNode.id );


			charNode = document.createElement( "span" );
			charNode.innerText = char;
			charNode.setAttribute( 'style', renderListData.listStyle );
			J( charNode ).css( 'paddingRight', '0px' );

		} else if( ListUtils.isImageList( dtNode )){

			charNode = document.createElement( "img" ); //NO I18N

			var imgData = dtNode.paraProperties.listData.listImageData,
				imgURL = imgData[ ATTR_IMAGE.I_SRC ];

			imgURL = HTMLUtil.getImageData( imgURL );

			var attrObj = {
					src		:	imgURL,
					width	:	imgData[ ATTR_IMAGE.I_WID ],
					height	:	imgData[ ATTR_IMAGE.I_HT ]
			};

			J( charNode ).attr( attrObj );
		}

		var charSpan = document.createElement( "span" ),
			spaceSpan = charSpan.cloneNode();

		charSpan.append( charNode, spaceSpan );
		spaceSpan.innerText = nbspChr;
		charSpan.setAttribute( "class", "zw-list-char" );

		_appendListChar( domPara, charSpan );
	},

	_appendListChar = function( domPara, charNode ){

		//append list char
		var startCommentNode = document.createComment("Zoho writer list char starts"), //No I18N
		endCommentNode = document.createComment("Zoho writer list char starts ends");//No I18N

		J( domPara ).prepend( startCommentNode, [ charNode, endCommentNode ]);
	},

	getPortionNode = function( dtRun, fieldData ){

		var dtPara = dtRun.parent,
		dtParaStart = dtPara.getIndex(),
		posStart = dtParaStart + dtRun.start,
		posEnd = dtParaStart + dtRun.end,
		char = editor.doc.getCharAt( posStart ),
		fmt = editor.doc.getComputedFormatAt( posStart,NODE_TYPE.TEXT ),clsData,
		type = fmt.type,
		node;

		switch( type ){
			case NODE_TYPE.TAB	:
			case NODE_TYPE.TEXT :	var _infoAttr, _infoCont = _getContent( posStart, posEnd ),
									frmt = DocUtil.getComputedTextFormat( posStart ),
									styleElements = {};

									node = DomUtil.createSpan( frmt );
									//TODO
									/*getFormat() is not providing with the format,
									 *made a temporary( to be modified ) fix with another function call
									 */
									var formatArrayKeys = Object.keys( ZWJSONUtils.jsonCleanser( DocUtil.getTextStyle( posStart ) ) );//Object.keys( dtRun.getFormat() );

									if( (fmt.st && fmt.st == NODE_TYPE.TAB) || (fmt.type && fmt.type == NODE_TYPE.TAB) ) {
										var tabAttr = {
														"leader": fmt.leader,//No I18N
														"cls": "Apple-tab-span",//No I18N
														"st": NODE_TYPE.TAB,//No I18N
														"type": NODE_TYPE.TEXT//No I18N
														}
										_infoCont  = UNICODE.TAB;
										_infoAttr  = {
														"data-text-info": JSON.stringify( tabAttr )//No I18N
														}
										styleElements = { "white-space": "pre" }; // NO I18N
									} else if( fmt.cls && ( fmt.cls == "footnote" || fmt.cls =="endnote" )){//No I18N

										_infoCont = "#";
										_infoAttr = {};
										_infoAttr[ "data-" + fmt.cls + "-id" ] = fmt.id; // NO I18N
										_infoAttr[ "data-text-info" ] = JSON.stringify( { "cls" : fmt.cls });// NO I18N

										if ( fmt.cls == "footnote" ) {
											_infoAttr[ "data-footnote-info" ] = JSON.stringify( { "footnote" : editor.doc.getBodyFormat().footnote });//NO I18N
										} else {
											_infoAttr[ "data-endnote-info" ] = JSON.stringify( { "endnote" : editor.doc.getBodyFormat().endnote });//NO I18N
										}

										styleElements = {
											'vertical-align' : 'super'	//NO I18N
										};
									} else if(	fmt.cls && ( fmt.cls.indexOf( CONST.MERGEFIELD ) !=-1 ||
												fmt.cls.indexOf( CONST.NEXTRECORD )) != -1 ){
										clsData = { "cls"	:	fmt.cls };//No I18N
										if( fmt.cls.indexOf( CONST.NEXTRECORD  ) != -1 || fmt.cls.indexOf( CONST.MERGEFIELD ) != -1 ){

											_infoAttr = {
															"data-mergefield-info": JSON.stringify( fieldData ), //NO I18N
															"data-text-info": JSON.stringify( clsData )//No I18N
														};
										}

									} else if( fmt.cls && HTMLUtil.getAutoFieldClass( fmt.cls )){
										var fieldInfo = fmt.fieldinfo,
										innerValue,
										cls = HTMLUtil.getAutoFieldClass( fmt.cls );
										if ( cls === "autopcnt") {
	                                        innerValue = ViewManager.getPageCount();
	                                    } else if ( cls === "autopno") {//No I18N
	                                        var view = ViewManager.getViews( dtPara )[0];
	                                        innerValue = (view && view.page) ? view.page.pageNumber : 1;
	                                    } else if( cls === "autoformula" ){ //No I18N
	                                    	innerValue = (/^(#)+$/.test(_infoCont)) ? dtPara.viewProperties.fields[ fieldInfo ].value : _infoCont;
	                                    } else if ( cls === "autodocname" ) {//No I18N
	                                    	innerValue = (/^(#)+$/.test(_infoCont)) ? dtPara.viewProperties.fields[ fieldInfo ].value : _infoCont;
	                                    } else if ( cls === "autoauthor"){//no i18n
	                                    	innerValue = (/^(#)+$/.test(_infoCont)) ? dtPara.viewProperties.fields[ fieldInfo ].value : _infoCont;
	                                    } else if ( cls === "autodocversion"){//no i18n
	                                    	innerValue = (/^(#)+$/.test(_infoCont)) ? dtPara.viewProperties.fields[ fieldInfo ].value : _infoCont;
	                                    } else {
	                                        innerValue = dtPara.viewProperties.fields[ fieldInfo ];
	                                    }
										_infoCont = innerValue;

										clsData = { "cls"	:	cls };//No I18N
										_infoAttr = {
												"data-autofield-info"	:	JSON.stringify( fieldData ), //NO I18N
												"data-text-info"		:	JSON.stringify( clsData )//No I18N
										};
									} else if( fmt.cls && fmt.cls == NODE_TYPE.FILLABLEFIELD ){
                                        /*
                                        clsData = { "cls"    :    fmt.cls };//No I18N
                                        _infoAttr = {
                                                "data-fillableField-info"    :    JSON.stringify( fieldData ), //No I18N
                                                "data-text-info"        :    JSON.stringify( clsData )//No I18N
                                        };
                                        */
                                        node = HTMLUtil.convertFillableFieldsToStdHTML( fmt, fieldData );

									} else if( fmt.cls && fmt.cls == NODE_TYPE.SIGNERFIELD ){

                                        clsData = { "cls"    :    fmt.cls };//No I18N
                                        _infoAttr = {
                                                "data-signerfield-info"    :    JSON.stringify( fieldData ), //No I18N
                                                "data-text-info"        :    JSON.stringify( clsData )//No I18N
                                        };

									} else if( fmt.cls && fmt.cls.indexOf( NODE_TYPE.LINK ) != -1 ){ //P8-4690 & P8-4691

										/* link text color should be drawn from theme.color.link. But,
										 * We yet to implement this in rendering. Till then, while copy
										 * operation highest precedence given to color in format array followed
										 * by default 'blue' ( html link color );
										 */

										if( dtRun.format && !dtRun.format.hasOwnProperty( ATTR_TEXT.TS_FGC )){
											//frmt[ ATTR_TEXT.TS_FGC ] = 'rgb( 0, 0, 255 )';
											delete frmt[ ATTR_TEXT.TS_FGC ];
										}

									}


									node.removeAttribute( 'class' ); //NO I18N
									node.textContent = _infoCont;
									_infoAttr  &&  J( node ).attr(  _infoAttr  );
									styleElements && J( node ).css( styleElements );

									if( dtRun.parent.isCode() ){
										node.setAttribute( 'data-format-attr', JSON.stringify( formatArrayKeys ) );
										//since the parent pre tag has the coressponding theme font color - span tag should not contain the color inline
										node.style.color = "";
									}

									break;

			case NODE_TYPE.IFRAME	:
									node = DomUtil.createIframe( fmt );
		                        	break;

			case NODE_TYPE.IMAGE	:	
										node = ( fmt.hasOwnProperty( ATTR_WIDGET.SRC )) ? DomUtil.createWidget( fmt ) : DomUtil.createImage( fmt );
										var attrs = {},
											styleElements = {};

										if( fmt.hasOwnProperty( ATTR_WIDGET.SHEET_INFO) ){
											attrs[ 'data-sheet-info' ] = JSON.stringify( fmt[ ATTR_WIDGET.SHEET_INFO ] );
										}
	
										if( fmt.hasOwnProperty( ATTR_IMAGE.I_CROPRECT  ) ){
											attrs[ 'data-crop-info' ] = JSON.stringify( fmt[ ATTR_IMAGE.I_CROPRECT ] );
			                        	}
	
										if( fmt.hasOwnProperty( ATTR_IMAGE.BARCODE_INFO  ) ){
											attrs[ 'data-barcode-info' ] = JSON.stringify( fmt[ ATTR_IMAGE.BARCODE_INFO ] );
			                        	}
	
										if( fmt.hasOwnProperty( ATTR_IMAGE.QRCODE_INFO  ) ){
											attrs[ 'data-qrcode-info' ] = JSON.stringify( fmt[ ATTR_IMAGE.QRCODE_INFO ] );
			                        	}
	
										if( fmt.hasOwnProperty( ATTR_COMMON.SUBTYPE  ) ){
											attrs[ 'data-subtype-info' ] = JSON.stringify( fmt[ ATTR_COMMON.SUBTYPE ] );
											if( fmt[ ATTR_COMMON.SUBTYPE ] == "textbox" ){
					                        	node = document.createElement( 'span' );
												node.classList.add('zw-textbox-image');//NO I18N
					                        	
												node.innerText = "#";
												delete fmt.tempId;
												attrs[ "data-textbox-id" ] = fmt.id; // NO I18N
												attrs[ "data-text-info" ] = JSON.stringify( { "cls" : "textbox" });// NO I18N
												attrs[ "data-textbox-info" ] = JSON.stringify( fmt );//NO I18N
												
												styleElements[ 'vertical-align' ] = 'super';	//NO I18N
												
											}
			                        	}
	
				                        if ( fmt.src && ( fmt.src.indexOf( "image.do" ) === 0 || fmt.src.indexOf( "remoteImage.do" ) === 0 )){//No I18N
				                        	node.src = HTMLUtil.getImageData( fmt.src );
				                        }
	
				                        if( fmt.hasOwnProperty( ATTR_WIDGET.SERVICE ) && fmt[  ATTR_WIDGET.SERVICE ] == "YOUTUBE" ){ //video service
				                        	attrs[ 'data-video-data' ] = JSON.stringify( fmt );//NO I18N
				                        	node.classList.add('zw-video-image');//NO I18N
				                        }

				                        if( fmt.subType && fmt.subType == "chart" ){
				                        	attrs[ 'data-chart-info' ] = JSON.stringify({ //NO I18N
												                        		subType	: fmt.subType,
												                        		chart_id	: fmt.chart_id,
												                        		sheet_id	: fmt.sheet_id
											                        		});
				                        }
	
				                        if( fmt.hasOwnProperty( ATTR_IMAGE.I_SHAPEDATA) ){ //shape
				                        	attrs[ 'data-shapedata' ] = JSON.stringify( fmt[ ATTR_IMAGE.I_SHAPEDATA ]);
				                        	node.classList.add('zw-shape-image');//NO I18N
				                        	
				                        }
	
				                        if( fmt.hasOwnProperty( 'mminfo' )){
				                        	attrs[ 'data-mminfo' ] = JSON.stringify( fmt );//NO I18N
				                        	node.classList.add('zw-mminfo-image');//NO I18N
				                        }
				                        
				                        J( node ).attr( attrs ).css( styleElements );
				                        
				                        break;

			case NODE_TYPE.BR	:	node = DomUtil.createBR();
									break;
			case NODE_TYPE.MAILMERGEFIELD:
									break;
			case NODE_TYPE.COMMENT	:
									node = document.createElement( "span" ); //No I18N
									var char = editor.doc.getCharAt( posStart );
									J( node ).attr( {
										"data-range-char-type"	:	char === UNICODE.RANGE_START ? "start" : "end",//No I18N
										"data-comment-info"	:	JSON.stringify( fmt ),//No I18N
										"data-range-id" : fmt.id //NO I18N
									});
									node.innerHTML = "&nbsp;";//No I18N
									break;
			case NODE_TYPE.NAMED_RANGE	:
			case NODE_TYPE.BOOKMARK	:
									node = document.createElement( "span" ); //No I18N
									J( node ).attr( {
										"data-range-char-type"	:	char === UNICODE.RANGE_START ? "start" : "end",//No I18N
										"data-bookmark-info"	:	JSON.stringify( fmt ),//No I18N
										"data-bookmark-id"		:	fmt.id//No I18N
									});
									node.innerHTML = "&nbsp;";//No I18N
									break;
			case NODE_TYPE.FILLABLEFIELD:
			case NODE_TYPE.FOOTNOTE		:
			case NODE_TYPE.ENDNOTE		:
			case NODE_TYPE.AUTOFIELD	:
								break;
			case NODE_TYPE.SIGNERFIELD	:
								break;
			case NODE_TYPE.LINK			:	if( editor.doc.getCharAt( posStart ) == UNICODE.RANGE_START ){
												var node = document.createElement( "a" ), href;
												if( fmt.lnktype == LINK_TYPES.BOOKMARK ){
													href = BookmarkDialog.getLink( fmt.url ) ;
													J( node ).attr( { "data-bookmark-name" : fmt.url }); //NO I18N
												}else {
													href = fmt.url;
												}

												fmt.lnktype == LINK_TYPES.MMFIELD ?
														J( node ).attr( { "data-mminfo" : JSON.stringify( fmt.mminfo ) }) : "";

												J( node ).css( { color : 'rgb( 0, 0, 255 )' } );
												node.setAttribute( "href", href );
											} else if( editor.doc.getCharAt( posStart ) != UNICODE.RANGE_END ){//P8-5965
												node = DomUtil.createSpan( fmt );
												node.textContent = _getContent( posStart, posEnd );
											}
											break;
			case  NODE_TYPE.WIDGET		:	node = DomUtil.createWidget( fmt );
			 								break;

			case NODE_TYPE.CONDITIONAL_ELSE_SEPARATOR		:
			case NODE_TYPE.CONDITIONAL_IF_END	:
			case NODE_TYPE.CONDITIONAL_IF_START	:	node = document.createElement( "span" ); //No I18N
													var char = editor.doc.getCharAt( posStart ),
													attrJSON = {};
													if( char == UNICODE.RANGE_START || char == UNICODE.RANGE_END ){

														attrJSON[ "data-range-char-type" ] = char === UNICODE.RANGE_START ? "start" : "end";//No I18N
														attrJSON[ "data-condition-info" ] = JSON.stringify( fmt );//No I18N
														attrJSON[ "data-condition-id" ] = fmt.id;//No I18N
														//attrJSON[ "range-id" ] = fmt.id //NO I18N

													} else if( type == NODE_TYPE.CONDITIONAL_ELSE_SEPARATOR ){

														attrJSON[ "data-range-char-type" ] = "else";//No I18N
														attrJSON[ "data-condition-id" ] = fmt.id;//No I18N

													}
													node.innerHTML = "&nbsp;";//No I18N

													J( node ).attr( attrJSON );
													break;
		}

		return node;
	},

	percentBasedWidth = function( cells, dataCols, dataWidth ){
		var len = cells.length, colElem = [],
        unit = '%', newDataCols = [], width = 0;
        for( var idx = 0; idx < len; idx++ ){ // finding the total width percentage of the table to be pasted
            var colVal, cell = cells[ idx ],
            colIdx = cell.getAttribute( 'col-index' );
            colVal = parseFloat( dataCols[ colIdx ].wt );
            colVal = colVal * ( dataWidth / 100 );
            colElem.push( colVal );
            width += colVal;
        }
        /*calculating the cols width percentage,
         *based on the calculated total width percentage*/
        for( var idx = 0; idx < len; idx++ ){
            var individualColPercent = ( colElem[ idx ] * 100 ) / width,
            colPercent = { 'wt': individualColPercent + unit };// NO I18N
            newDataCols.push( colPercent );
        }
        return {
        	'dataCols' 	: newDataCols, //NO I18N
        	'width' 	: width //NO I18N
        };
	},

    unitBasedWidth = function( cells, dataCols ){
    	var newDataCols = [], width = 0;
        for( var idx = 0, len = cells.length; idx < len; idx++ ){
            var colElem, cell = cells[ idx ],
            colIdx = cell.getAttribute( 'col-index' );
            colElem = dataCols[ colIdx ];
            newDataCols.push( colElem );
            width += parseFloat( colElem.wt );
        }
        return {
        	'dataCols' 	: newDataCols, //NO I18N
        	'width' 	: width //NO I18N
        };
    },

	processTableElemsForCopy = function( tableEle ){

        var dataCols = JSON.parse( tableEle.getAttribute( 'data-cols' ) ),
        cells, attr, newDataCols,
        width = 0, unit, unitVariable, firstRow, colsWidth,
        dataWidth = parseFloat( tableEle.getAttribute( 'data-width' ) );

        firstRow = tableEle.firstElementChild;
        cells = firstRow.cells;
       // unitVariable = dataCols[ cells[ 0 ].getAttribute( 'col-index' ) ];

        if( dataCols[0].wt.indexOf('%') != -1 ){
        	colsWidth = percentBasedWidth( cells, dataCols, dataWidth );
        	unit = '%';
        } else {
        	colsWidth = unitBasedWidth( cells, dataCols );
        	unit = dataCols[0].wt.indexOf( 'mm' ) != -1 ? 'mm' : 'in'; //NO I18N
        }

        width = colsWidth.width.toFixed( 2 ) + unit;
        newDataCols = colsWidth.dataCols;
        attr = {
                'data-width' : width, //NO I18N
                'data-cols' : JSON.stringify( newDataCols ) //NO I18N
        };
        J( tableEle ).attr( attr );
        //remove hmp vmp
        J( tableEle ).find( '[merge="vmp"]' ).remove();
        J( tableEle ).find( '[merge="hmp"]' ).remove();

	};


	return {
		getParagraph:	getParagraph,
		getTable	:	getTable,
		getCell		:	getCell,
		getPortionNode	:	getPortionNode,
		getHR		:	getHR,
		getRow		:	getRow,
		createListElement	:	createListElement,
		processTableElemsForCopy : processTableElemsForCopy,
		paraStyleKeyDataAttrMap : _paraStyleKeyDataAttrMap
	};
})();