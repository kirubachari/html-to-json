/*$Id$*/

//Number related self-executive block
(function() {
	
	//Number.prototype extensions
	
	Number.prototype.convertor = function() {
	    return {
	        inches: this.valueOf(),
	        pixels: this.valueOf(),
	        centimeters: this.valueOf(),
	        millimeters: this.valueOf(),
	        points: this.valueOf(),
	        picas: this.valueOf()
	    };
	};
	
	//TODO: To be discussed with @Sivaguru by @Gokul
    var UNIT_VALUE_PER_INCH = {
    		"px": 96,//no i18n
    		"in": 1,//no i18n
    		"cm": 2.54,//no i18n
    		"mm": 25.4,//no i18n
    		"pt": 72,//no i18n
    		"pc": 862//no i18n
    	};
    
    Number.prototype.isBetween = function(min, max, inclusive) {
    	return inclusive ? (min <= this && this <= max) : (min < this && this < max);
    };
    
    Number.prototype.contains = function(value) {
    	return ((this & value) == value);
    };
    
    Number.prototype.convertUnit = function(fromUnit, toUnit) {
    	return this * UNIT_VALUE_PER_INCH[toUnit]/UNIT_VALUE_PER_INCH[fromUnit];
    };
	
}());