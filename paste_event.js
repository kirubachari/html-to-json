var _getPasteEventInfo = function(clipboardData) {

        var htmlString = clipboardData.getData("text/html"), //No i18n
            textString = clipboardData.getData("text/plain"), //No i18n
            imageItem = null;
        if (clipboardData.files && clipboardData.files.length) { //image Paste
            var clipboardItemCount = clipboardData && clipboardData.items && clipboardData.items.length;
            if (clipboardItemCount > 0) {
                var idx = 0,
                    item;
                while (idx < clipboardItemCount) {
                    item = clipboardData.items[idx];
                    if (item && item.type.indexOf("image") != -1) {
                        imageItem = item.getAsFile(); //handle this
                        break;
                    }
                    idx++;
                }
            }
        }

        return {
            'html': htmlString, //No I18N
            'text': textString, //No I18N
            'imageItem': imageItem //No I18N
        };

    },

    handlePasteEvent = function(e) {

        var oriEvent = e.originalEvent,
            clipboardInfo = {},
            clipboardData = (oriEvent && oriEvent.clipboardData) ? oriEvent.clipboardData : e.clipboardData,
            textString, htmlString, imageItem;

        if (clipboardData) {
            clipboardInfo = _getPasteEventInfo(clipboardData);
            console.log(clipboardInfo);
            _writeContentInFrame(clipboardInfo);
            var htmlFrm = document.getElementById("pasteFrame"),
                body = htmlFrm.contentWindow.document.body;
            
            console.log( JsonTreeBuilder.parseHTML( [body]));
        }
    },

    bindPaste = function(){ 
        const target = document.querySelector('div.target');
        target.addEventListener('paste', handlePasteEvent);
    },

    _writeContentInFrame = function( pasteInfo, callback ){
            
        var htmlString = pasteInfo.html,
            textString = pasteInfo.text,
            imageItem = pasteInfo.imageItem,
            htmlFrm = document.getElementById("pasteFrame"),
            textFrm = document.getElementById("textFrame"),
            deferredArr = [];

        _setContentInIframe( htmlFrm, htmlString );
    }

    _setContentInIframe = function( frm, content, isTextOnly ){
        
        frm.contentWindow.document.open();
        frm.contentWindow.document.write(content);
        frm.contentWindow.document.close();
    }