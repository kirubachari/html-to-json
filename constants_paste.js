/* $Id$ */
UNICODE = {
		    TABLE           : '\u0010',//No I18n
		    ROW             : '\u0012',//No I18n
		    CELL            : '\u001c',//No I18n
		    TABLEEND        : '\u0011',//No I18n
		    IMAGE           : '\u0013',//No I18n
		    IFRAME	    : '\u0013',//No I18n
		    BR              : '\u000b',//No I18n
		    PARA            : '\n',//No I18n
		    PARAGRAPH       : '\n', //no i18n
		    TAB             : '\t',//No I18n
		    RANGE_START     : '\u000e',//No I18n
		    RANGE_END       : '\u000f',//No I18n
		    FIELD_SEPARATOR : '\u0014', //NO I18N
		    SPACE           : '\u0020',//No I18n
		    MULTIBYTE_SPACE : '\u3000', //No I18N
		    SECTION_START   : '\u0003',//No I18n
		    HR              : '\r',//No I18N
		    TOCEND          : '\ueeff',//No I18n
		    TOCSTART        : '\ueefe',//No I18N
		    MERGEFIELD_START : '\u00AB',//No I18N
		    MERGEFIELD_END : '\u00BB'//No I18N
		};
	
NODE_TYPE = {
		    BODY            :       "body",//No I18n
		    PARA            :       "para", //No I18n
		    PARAGRAPH       :       "paragraph",    //No I18n
		    TAB             :       "tab",  //No I18n
		    TEXT            :       "text",//No I18n
		    IMAGE           :       "image",//No I18n
		    IFRAME          :       "iframe",//No I18n
		    HR              :       "hr",   //No I18n
		    BR              :       "br",   //No I18n
		    TABLE           :       "table",//No I18n
		    TABLEEND        :       "tableend", //NO I18N
		    TBODY           :       "tbody",//No I18n
		    ROW             :       "row",//No I18n
		    RUN             :       "run", //No I18N
		    BOOKMARK        :       "bookmark",  //No i18n
		    FIELD_START     :       "fieldstart", //No I18N
		    FIELD_END       :       "fieldend", //No I18N
		    CELL            :       "cell",//No I18n
		    LINK            :       'link',//No I18n        
		    WORD            :       'word',//No I18n
		    HEADER_PRIMARY  :       "header_primary",//No I18n
		    HEADER_FIRST    :       13,
		    HEADER_ODD      :       14,     
		    FOOTER_PRIMARY  :       "footer_primary",//No I18n
		    FOOTER_FIRST    :       16,
		    FOOTER_ODD      :       17,    
		    FOOTNOTE        :       "footnote",//No I18n
		    ENDNOTE         :       "endnote",//No I18n
		    COMMENT         :       "comment",//No I18n
		    SECTION         :       "section",  //No i18n
		    AUTOFIELD       :       "autofield", //No i18n
		    FILLABLEFIELD   :       "fillablefield", //No i18n
		    MAILMERGEFIELD  :       "mergeField",  //No i18n
		    NEXTRECORD      :       "nextrecord",  //No i18n
		    PLACEHOLDER     :       "placeholder",//No i18n
		    TOC             :       "toc",  //NO I18N
		    TOCEND          :       "tocend",    //NO I18N
		    WIDGET			:		"widget",//NO I18N
		    SIGNERFIELD		:		"signerfield",//No I18N
		    CONDITIONAL_IF_START    :       "conditional_if_start", //NO I18N
		    CONDITIONAL_IF_END	:	"conditional_if_end",//NO I18N
		    CONDITIONAL_ELSE_SEPARATOR	:	"conditional_else_seperator",//NO I18N
		    SHAPE			:		"shape", //No i18n
		    CHART			:		"chart",  //No i18n
		    NAMED_RANGE     :       "namedrange", //No I18N 
		    QRCODE			:		"QRCode", //No i18n
		    BARCODE			:		"BarCode" //No i18n
		};
	
	UNITS = {
		    PIXELS : 'px',          //NO I18N
		    MILLIMETERS : 'mm',     //NO I18N
		    INCHES : 'in',          //NO I18N
		    PERCENTAGE : '%'        //NO I18N
		};
var LIST_STYLE_OBJECT   =   {
        0   :   {"level0":{"type":0,"startsWith":1,"c":"\u0000."},"level1":{"type":4,"startsWith":1,"c":"\u0001."},"level2":{"type":2,"startsWith":1,"c":"\u0002."},"level3":{"type":0,"startsWith":1,"c":"\u0003."},"level4":{"type":4,"startsWith":1,"c":"\u0004."},"level5":{"type":2,"startsWith":1,"c":"\u0005."},"level6":{"type":0,"startsWith":1,"c":"\u0006."},"level7":{"type":4,"startsWith":1,"c":"\u0007."},"level8":{"type":2,"startsWith":1,"c":"\u0008."}},//NO I18N
        4   :   {"level0":{"type":4,"startsWith":1,"c":"\u0000."},"level1":{"type":2,"startsWith":1,"c":"\u0001."},"level2":{"type":0,"startsWith":1,"c":"\u0002."},"level3":{"type":4,"startsWith":1,"c":"\u0003."},"level4":{"type":2,"startsWith":1,"c":"\u0004."},"level5":{"type":0,"startsWith":1,"c":"\u0005."},"level6":{"type":4,"startsWith":1,"c":"\u0006."},"level7":{"type":2,"startsWith":1,"c":"\u0007."},"level8":{"type":0,"startsWith":1,"c":"\u0008."}},//NO I18N
        3   :   {"level0":{"type":3,"startsWith":1,"c":"\u0000."},"level1":{"type":2,"startsWith":1,"c":"\u0001."},"level2":{"type":0,"startsWith":1,"c":"\u0002."},"level3":{"type":4,"startsWith":1,"c":"\u0003."},"level4":{"type":2,"startsWith":1,"c":"\u0004."},"level5":{"type":0,"startsWith":1,"c":"\u0005."},"level6":{"type":4,"startsWith":1,"c":"\u0006."},"level7":{"type":2,"startsWith":1,"c":"\u0007."},"level8":{"type":0,"startsWith":1,"c":"\u0008."}},//NO I18N
        2   :   {"level0":{"type":2,"startsWith":1,"c":"\u0000."},"level1":{"type":0,"startsWith":1,"c":"\u0001."},"level2":{"type":4,"startsWith":1,"c":"\u0002."},"level3":{"type":2,"startsWith":1,"c":"\u0003."},"level4":{"type":0,"startsWith":1,"c":"\u0004."},"level5":{"type":4,"startsWith":1,"c":"\u0005."},"level6":{"type":2,"startsWith":1,"c":"\u0006."},"level7":{"type":0,"startsWith":1,"c":"\u0007."},"level8":{"type":4,"startsWith":1,"c":"\u0008."}},//NO I18N
        1   :   {"level0":{"type":1,"startsWith":1,"c":"\u0000."},"level1":{"type":2,"startsWith":1,"c":"\u0001."},"level2":{"type":0,"startsWith":1,"c":"\u0002."},"level3":{"type":4,"startsWith":1,"c":"\u0003."},"level4":{"type":2,"startsWith":1,"c":"\u0004."},"level5":{"type":0,"startsWith":1,"c":"\u0005."},"level6":{"type":4,"startsWith":1,"c":"\u0006."},"level7":{"type":2,"startsWith":1,"c":"\u0007."},"level8":{"type":0,"startsWith":1,"c":"\u0008."}},//NO I18N
        6   :   {"level0":{"type":6,"startsWith":1},"level1":{"type":7,"startsWith":1},"level2":{"type":8,"startsWith":1},"level3":{"type":6,"startsWith":1},"level4":{"type":7,"startsWith":1},"level5":{"type":8,"startsWith":1},"level6":{"type":6,"startsWith":1},"level7":{"type":7,"startsWith":1},"level8":{"type":8,"startsWith":1}},//NO I18N
        7   :   {"level0":{"type":7,"startsWith":1},"level1":{"type":8,"startsWith":1},"level2":{"type":6,"startsWith":1},"level3":{"type":7,"startsWith":1},"level4":{"type":8,"startsWith":1},"level5":{"type":6,"startsWith":1},"level6":{"type":7,"startsWith":1},"level7":{"type":8,"startsWith":1},"level8":{"type":6,"startsWith":1}},//NO I18N
        8   :   {"level0":{"type":8,"startsWith":1},"level1":{"type":6,"startsWith":1},"level2":{"type":7,"startsWith":1},"level3":{"type":8,"startsWith":1},"level4":{"type":6,"startsWith":1},"level5":{"type":7,"startsWith":1},"level6":{"type":8,"startsWith":1},"level7":{"type":6,"startsWith":1},"level8":{"type":7,"startsWith":1}},//NO I18N
        11  :   {"level0":{"type":0,"startsWith":1,"c":"\u0000."},"level1":{"type":0,"startsWith":1,"c":"\u0000.\u0001."},"level2":{"type":0,"startsWith":1,"c":"\u0000.\u0001.\u0002."},"level3":{"type":0,"startsWith":1,"c":"\u0000.\u0001.\u0002.\u0003."},"level4":{"type":0,"startsWith":1,"c":"\u0000.\u0001.\u0002.\u0003.\u0004."},"level5":{"type":0,"startsWith":1,"c":"\u0000.\u0001.\u0002.\u0003.\u0004.\u0005."},"level6":{"type":0,"startsWith":1,"c":"\u0000.\u0001.\u0002.\u0003.\u0004.\u0005.\u0006."},"level7":{"type":0,"startsWith":1,"c":"\u0000.\u0001.\u0002.\u0003.\u0004.\u0005.\u0006.\u0007."},"level8":{"type":0,"startsWith":1,"c":"\u0000.\u0001.\u0002.\u0003.\u0004.\u0005.\u0006.\u0007.\b."}}, //NO I18N
        255 :   {"level0":{"type":255,"startsWith":1},"level1":{"type":255,"startsWith":1},"level2":{"type":255,"startsWith":1},"level3":{"type":255,"startsWith":1},"level4":{"type":255,"startsWith":1},"level5":{"type":255,"startsWith":1},"level6":{"type":255,"startsWith":1},"level7":{"type":255,"startsWith":1},"level8":{"type":255,"startsWith":1}} //NO I18N
    };
