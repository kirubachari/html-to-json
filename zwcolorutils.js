/* $Id:$*/

var ZWColorUtils = (function(){
	
    var getColorAfterTint = function(rgb, tint) {
        //rgb would be rgb(xx, yy, zz)
        tint = tint / 100;
        var colorsOnly = rgb.substring(rgb.indexOf('(') + 1, rgb.lastIndexOf(')')).split(/,\s*/),
            hslValue = rgbToHsl(parseInt(colorsOnly[0]), parseInt(colorsOnly[1]), parseInt(colorsOnly[2]));

        hslValue[2] = hslValue[2] * tint + ( 1 - tint );
        var rgbColor = hslToRgb(hslValue[0], hslValue[1], hslValue[2]);
        return 'rgb(' + rgbColor[0] + ',' + rgbColor[1] + ',' + rgbColor[2] + ')';//NO I18n
    };
    
    var getColorAfterShade = function(rgb, shade) {
        //rgb would be rgb(xx, yy, zz)
        var colorsOnly = rgb.substring(rgb.indexOf('(') + 1, rgb.lastIndexOf(')')).split(/,\s*/),
            hslValue = rgbToHsl(parseInt(colorsOnly[0]), parseInt(colorsOnly[1]), parseInt(colorsOnly[2]));

        hslValue[2] = hslValue[2] * ( parseInt(shade) / 100 );
        var rgbColor = hslToRgb(hslValue[0], hslValue[1], hslValue[2]);
        return 'rgb(' + rgbColor[0] + ',' + rgbColor[1] + ',' + rgbColor[2] + ')';//NO I18n
    };
    
    var rgbToHsl = function(r, g, b) {
        r /= 255;
        g /= 255;
        b /= 255;
        var max = Math.max(r, g, b), min = Math.min(r, g, b);
        var h, s, l = (max + min) / 2;

        if (max == min) {
            h = s = 0; // achromatic
        } else {
            var d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
            switch (max) {
                case r:
                    h = (g - b) / d + (g < b ? 6 : 0);
                    break;
                case g:
                    h = (b - r) / d + 2;
                    break;
                case b:
                    h = (r - g) / d + 4;
                    break;
            }
            h /= 6;
        }
        return [h, s, l];
    };
    
    var hslToRgb = function(h, s, l) {
        var r, g, b;

        if (s == 0) {
            r = g = b = l; // achromatic
        } else {
            var hue2rgb = function hue2rgb(p, q, t) {
                if (t < 0) {
                    t += 1;
                }
                if (t > 1) {
                    t -= 1;
                }
                if (t < 1 / 6) {
                    return p + (q - p) * 6 * t;
                }
                if (t < 1 / 2) {
                    return q;
                }
                if (t < 2 / 3) {
                    return p + (q - p) * (2 / 3 - t) * 6;
                }
                return p;
            };

            var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;
            r = hue2rgb(p, q, h + 1 / 3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1 / 3);
        }
        return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
    };
    
    var invertColor = function(hexTripletColor) {
        var color = hexTripletColor;
        color = color.substring('#'.length); // remove #
        color = parseInt(color, 16); // convert to integer
        color = 0xFFFFFF ^ color; // invert three bytes
        color = color.toString(16); // convert to hex
        color = ("000000" + color).slice(-6); // pad with leading zeros
        color = "#" + color; // prepend #
        return color;
    };
    
    var componentToHex = function(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    };
    
    var rgbToHex = function(rgb) {
        if (rgb.indexOf('#') === 0) {
            return rgb;
        }

        var sCh = '(', eCh = ')';
        var rgbColor = rgb.substring(rgb.indexOf(sCh) + sCh.length, rgb.lastIndexOf(eCh));
        var colorValues = rgbColor.split(",");
        return "#" + componentToHex(parseInt(colorValues[0])) + componentToHex(parseInt(colorValues[1])) + componentToHex(parseInt(colorValues[2]));//No I18N
    };
    
    var rgbaToRgb = function(colorCssString) {
    	  var isRGBA = function(colorCssString) {
    	    return colorCssString.match(/rgba/gi);
    	  }
    	  if (!isRGBA(colorCssString)) {
    		  return colorCssString;
    	  }
    	  var colorArray = colorCssString.match(/\d+\.*\d*/g); 
    	  var Source = {};
    	  Source.R = colorArray.shift()/255;
    	  Source.G = colorArray.shift()/255;
    	  Source.B = colorArray.shift()/255;
    	  Source.A = colorArray.shift()/1; 
    	  var BGColor = {
    	    R: 1,
    	    G: 1,
    	    B: 1
    	  };
    	  var Target = {};
    	  Target.R = parseInt(255 * (((1 - Source.A) * BGColor.R) + (Source.A * Source.R)));
    	  Target.G = parseInt(255 * (((1 - Source.A) * BGColor.G) + (Source.A * Source.G)));
    	  Target.B = parseInt(255 * (((1 - Source.A) * BGColor.B) + (Source.A * Source.B))); 
    	  var rgbColor = "rgb("+Target.R+","+ Target.G+","+Target.B+")"; //NO I18N
    	  return rgbColor;
    	};
	return {
		getColorAfterTint	: getColorAfterTint,
		getColorAfterShade	: getColorAfterShade,
		rgbToHsl			: rgbToHsl,
		hslToRgb			: hslToRgb,
		invertColor			: invertColor,
		rgbToHex			: rgbToHex,
		rgbaToRgb			:rgbaToRgb
	}
})();