/* $Id$ */
var ZWJSONUtils = (function(){
	"use strict";//no i18n
	
	var isPlainObject = function( obj ){
    	return	typeof obj === 'object' //no i18n
    		&& obj !== null
    		&& obj.constructor === Object
    		&& Object.prototype.toString.call(obj) === '[object Object]';
    },
    
    isEmptyObject = function( obj ){
    	return Object.keys( obj ).length === 0 && obj.constructor === Object;
    },
    
    isJSON = function( str ) { 
        try {
            return ( JSON.parse( str ) && true );
        } catch ( e ) {
            return false;
        }
    },
    
    clone = function(obj) {
        var copy;
 
        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) { return obj; }
 
        // Handle Array
        if (Array.isArray(obj)) {
            copy = [];
            for(var i = 0, len = obj.length; i < len; i++){
                copy[i] = clone(obj[i]);
            }
            return copy;
        }
 
        // Handle Object
        if (isPlainObject(obj)) {
            copy = {};
            for(var attr in obj){
                if (obj.hasOwnProperty(attr)) { copy[attr] = clone(obj[attr]); }
            }
            return copy;
        }
 
        //throw new Error("Unable to copy obj! Its type isn't supported.");
        return copy;
    },
    
    mergeJsonObjs = function(obj1, obj2) {
        for (var key in obj2) {
            if(obj2.hasOwnProperty(key)) {
                obj1[key] = obj2[key];
            }
        }
    },
    
    emptyJSON = function( json ){
        for( var propName in json ) { 
        	delete json[ propName ];
        }
    };
		
	return {
		isPlainObject	:	isPlainObject,
		isEmptyObject	:	isEmptyObject,
		clone			:	clone,
		mergeJsonObjs	:	mergeJsonObjs,
		isJSON			:	isJSON,
		emptyJSON		:	emptyJSON
	}
})();